---
layout: default
main_title: 代表取締役　須藤功平が次世代データ処理基盤Apache ArrowのPMC chairに就任
sub_title:
type: press
---

<p class='press-release-subtitle-small'>
クリアコードは、Apache Arrowコンサルティングサポートを提供しています。
</p>

<div class="press-release-signature">
  <p class="date">2022年3月3日</p>
  <p>株式会社クリアコード</p>
</div>



株式会社クリアコード（本社：埼玉県所沢市、代表取締役：須藤 功平)は、代表取締役　須藤功平が世界中から開発者が参加しているオープンソースソフトウェア「Apache Arrow」[^1]のPMC（Project Management Committee・プロジェクト管理委員会）chairに就任したことをお知らせします。

[^1]: Apache Arrow, the Apache Arrow project logo are either registered trademarks or trademarks of The Apache Software Foundation in the United States and other countries.（Apache Arrowの商標およびロゴはApache Software Foundationの商標です。）

Apache Arrowは、大規模データの交換処理を効率化するソフトウェアで次世代データ処理基盤として期待されています。Apache Arrowは10以上のプログラミング言語で利用できるため、異なるプログラミング言語で実装された各種データ処理プロダクトを効率よく連携できます。2016年から開発が進み、2020年7月の1.0.0リリースをきっかけに様々なプロジェクトで採用が進みました。2022年3月現在の最新バージョンは7.0.0です。

Apache Software Foundation[^2]傘下であるApache Arrowプロジェクトは、活発で健全なプロジェクト開発のため、PMC（Project Management Committee・プロジェクト管理委員会）がプロジェクトを管理します。PMC chairはPMCの代表です。


[^2]: [Apache Software Foundation](https://www.apache.org/)は、オープンソースソフトウェアプロジェクトを支援するアメリカ合衆国に登録されている非営利団体です。現在支援を行っている傘下のプロジェクトおよび個人の数は、350以上に上ります。傘下のプロジェクトに対して知的財産権利の守り方や財政支援に関するフレームワークを提供し、コミュニティの健全な運営及び、開発推進の支援を行っています。

**プロフィール**

須藤　功平

* 2008年 8月 株式会社クリアコード　代表取締役就任
* 2016年12月 Apache Arrowの開発に参加
* 2017年 5月 Apache Arrowコミッターに就任
* 2017年 9月 Apache Arrow PMCメンバーに就任
* 2018年 1月 Apache Arrowを使い、Rubyでデータ処理を行うためのプロジェクトRed Data Tools 立ち上げ
* 2022年 1月 Apache Arrow PMC chairに就任

2022年3月時点でコミット数は3位。日本でApache Arrowを普及させるため、開発するだけでなく各所でApache Arrowについて紹介しています。

【Apache Arrow関連講演事例】

* [『Apache Arrowフォーマットはなぜ速いのか』](https://bit.ly/3wnOwLH)
* [『Apache Arrow 1.0 - A cross-language development platform for in-memory data』](https://bit.ly/3mTK58j)
* [『Red Arrow - Ruby and Apache Arrow』](https://bit.ly/3mR9QWH)

## クリアコードのApache Arrowコンサルティングサポート

クリアコードでは、 Apache Arrowによる大規模データ交換の効率化を目指すお客様や、 Apache Arrowをどのように活用したらよいか相談に乗って欲しい、 Apache Arrowを使っていて解決したい点があるお客様に対して、コンサルティングサポートを提供しています。

開発当初からプロジェクトに関わっている須藤をはじめ、FluentdやGroongaといったデータを扱う様々なOSS開発・メンテナンスに関わっている経験豊かなエンジニアが、Apache Arrowの活用だけではなく、実際の運用におけるデータ処理に関する課題や期待をヒアリングしたうえで、データ処理ツールの開発などを含めたコンサルティングサポートを提供します。まずは、お気軽にお問合せください。


### Apache Arrowの概要

大容量のストレージ・大量のコンピューターリソースを活用することが可能になった現在では、従来は現実的でなかった大量データを処理できるようになりました。現代のデータ処理システムは複数のデータ処理プロダクトを連携して構築されており、データ量の多い環境においては各モジュール間でのデータ交換を効率化することが必須です。この効率的なデータ交換のために設計されたデータフォーマットが「Apache Arrow」です。たとえば、従来はCSVやJSONを使っていたところをApache Arrowを使うようにすればデータ交換処理に必要なコンピューターリソースは非常に小さくなり、本来のデータ処理にコンピューターリソースを投入できます。

<div class="text-center">

![Apache Arrowでデータ交換を高速化]({% link services/apache-arrow/data-exchange.png %})

</div>

実は、Apache Arrowフォーマットによる高速化はApache Arrowが提供する機能の1つにすぎません。Apache Arrowはメモリー上でデータ処理するために必要な一連の機能を提供します。Apache Arrowフォーマットによるデータ交換の高速化以外にも次のような機能があります。
* CPU上でSIMD・JITコンパイルを使って高速にデータを処理する機能
* GPU上で高速にデータを処理する機能
* CSVやApache Parquetなど既存のデータフォーマットと相互変換する機能
* 高速なRPC機能
従来はこのような機能は各データ処理プロダクトで個別に実装されていましたが、Apache Arrowは各データ処理プロダクトで共有できる高速な実装を提供します。


### 活用事例
Apache Spark™をはじめApache Arrowを利用して高速化したデータ処理プロダクトが増えています。Apache ArrowのPythonライブラリーであるpyarrowだけでも、2020年8月23日の1日のダウンロード数は約23万件です。

以下にApache Arrowの利用例を紹介します。

* [Amazon Athenaの新しいフェデレーテッド・クエリによる複数データソースの検索 | Amazon Web Services ブログ](https://aws.amazon.com/jp/blogs/news/query-any-data-source-with-amazon-athenas-new-federated-query/)：Amazon Athenaへのデータ提供時にApache Arrowを利用
* [PostgreSQLだってビッグデータ処理したい！！～GPUとNVMEを駆使して毎秒10億レコードを処理する技術～ ](https://www.slideshare.net/kaigai/20191115pgconfjapan)：PostgreSQLで毎秒10億レコードを処理するためにApache Arrowを利用
* [Vaex: A DataFrame with super strings | by Maarten Breddels | Towards Data Science](https://towardsdatascience.com/vaex-a-dataframe-with-super-strings-789b92e8d861)：Pythonのデータフレームの文字列処理の高速化にApache Arrowを利用
* [Powered by | Apache Arrow](https://arrow.apache.org/powered_by/)：Apache Arrow公式サイトに集められた利用例集



## クリアコードについて
クリアコードは、 2006年7月にフリーソフトウェア開発者を中心に設立したソフトウェア開発会社です。クリアコードの目的は、単に会社を継続していくことではありません。フリーソフトウェアの開発で学んだことを継続的にビジネス分野に活用していくことで会社を継続し、それと同時に、ビジネスを継続することでフリーソフトウェアへ継続的にコミットメントしていくこと、この両立の実現が当社の目的です。この理念は、我々がフリーソフトウェアの開発で学んだことがベースとなっています。現在、Apache Arrow・Groonga・Fluentdといった様々なソフトウェアのコアメンテナが在籍しています。


### 参考URL
【コーポレートサイト】[{{ site.url }}{% link index.html %}]({% link index.html %})

【本プレスリリース】[{{ site.url }}{% link press-releases/20220303-apache-arrow-pmc-chair.md %}]({% link press-releases/20220303-apache-arrow-pmc-chair.md %})

【関連サービス】[{{ site.url }}{% link services/apache-arrow.md %}]({% link services/apache-arrow.md %})


### 当リリースに関するお問合せ先
株式会社クリアコード

TEL：04-2907-4726

メール：info@clear-code.com
