---
tags:
  - apache-arrow
  - ruby
title: '名古屋Ruby会議03：Apache ArrowのRubyバインディング（3） #nagoyark03'
---
[前回はなぜApache ArrowのRubyバインディングがあるとよさそうかについて説明]({% post_url 2017-01-20-index %})しました。今回はなぜApache ArrowのRubyバインディングを[GObject Introspection](https://wiki.gnome.org/Projects/GObjectIntrospection)で作るとよさそうかについて説明します。
<!--more-->


理由は、Apache Arrowを複数のプログラミング言語から使えるようになることと、バインディングのメンテナンスをしやすくなるからです。

GObject IntrospectionはCライブラリーの各種言語バインディングを作るためのライブラリーです。それぞれの言語バインディングは自動生成するため、GObject Introspectionに対応すれば個別に各言語のバインディングを用意する必要はありません。たとえば、Ruby用のバインディングとLua用のバインディングを個別に用意する必要はありません。GObject Introspectionに対応すればどちらのバインディングも使えるようになります。

もし、[SWIG](http://www.swig.org/)を知っているのであれば、GObject Introspectionに対応することで得られる効果はSWIGを使った場合と同じと思えばだいたいあっています。

Rubyでもデータ分析をしたいのであれば、拡張ライブラリーとしてApache Arrowのバインディングを作るというやり方もあります。それぞれの違いは次の通りです。

GObject Introspectionを使うやり方：

  * Ruby以外の言語でもApache Arrowを使えるようになる。

  * Apache ArrowをGObject Introspectionに対応させる人はGObject Introspectionについて知らないといけない。（使う人は知らなくてもよい。）

拡張ライブラリーとして作るやり方：

  * RubyでだけApache Arrowを使えるようになる。

  * 拡張ライブラリーを作る人は拡張ライブラリーについて知らないといけない。（使う人はあまり知らなくてもよい。）

知らないといけない知識が変わるという違いもありますが、ポイントはRuby以外の言語でもApache Arrowを使えるようになるかという点です。

Ruby以外の言語でもApache Arrowを使えるようになると、Rubyだけでしか使えないときよりもうれしい人は多くなり、メンテナンスするメリットがある人も多くなります。そうすると1人あたりのメンテナンスコストを下げられる可能性があります。

Apache Arrowが本当に多くのデータ分析プロダクトで使われるようになると、Ruby以外の言語でもバインディングが必要とされるでしょう。そうなったときにメンテナンスコストを複数人で分配できるとうれしいです。

現状ではデータ分析をするならJava、Python、Rあたりを使います。これらの言語はユーザーが多いため、今後もこれらの言語のデータ分析まわりは活発に改良されていくでしょう。一方、他の言語はユーザーが少なくあまり改良されていかないでしょう。Java、Python、R以外の言語でも必要に応じてデータ分析に使うようになれば状況は変わるかもしれません。バインディングが用意されることによりApache Arrowがいろんな言語で使えるようになるとそうなるかもしれません。

次回からは実際にGObject Introspectionを使ってApache Arrowのバインディングを作る方法を説明します。
