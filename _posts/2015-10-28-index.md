---
tags: []
title: WerckerのDockerベーススタックへの移行
---
クリアコードでは[Hatohol](http://www.hatohol.org)というソフトウェアの開発に参加しています。Hatoholは複数の統合監視システムの情報を一括して表示することを可能とするソフトウェアです。現在対応している統合監視システムはZabbix、Nagios及びOpen StackのCeilometerです。他の監視システムに対応することも検討しています。
<!--more-->


HatoholはServerとDjangoのWebApp、またユーザーのブラウザで動くJavaScript部分の3つで構成されています。

前回、Hatohol serverの [CentOS向けのCIでDockerfileを用い、Werckerで行う方法 - ククログ(2015-07-01)]({% post_url 2015-07-01-index %})を紹介しました。

その後、WerckerのビルドスタックがDockerベースのものに切り替わったため、移行作業を行いました。

### Werckerとは

[Wercker](http://wercker.com)とはTravis CIやCircle CIと並ぶCIサービスです。特に、ビルドスタックが使うコンテナを自由にユーザーが用意する事ができ、自由度が高いのが特徴です。

今回Werckerが[Dockerベースのバックエンドを公開](http://blog.wercker.com/2015/04/02/Introducing-our-docker-stack.html)したので、HatoholのCentOS 6系向けCIをこの新しいビルドスタックで行うように移行作業を行いました。

### Dockerベースのビルドスタックを使用する

Werckerはビルド時の設定をwercker.ymlで行います。box名のDockerイメージがあるかどうかをDockerHubに問い合わせるようになっています。

```yaml
box: centos:6.7
```


例えば、上記のbox名がwercker.ymlに指定されていた場合、DockerHubにcentos:6.7のコンテナがあるかどうか問い合わせに行きます。

boxへDockerHubのイメージが指定出来るようになったことにより、Wercker専用のboxを作らずともDockerHubに登録されているDockerイメージを自由に選択することが出来るようになりました。

### Dockerベースのビルドスタックへの移行

それでは本題です。Dockerベースのビルドスタックに移行した場合は、`wercker-labs/docker` ではDockerfileを直接使っていましたが、Dockerベースのビルドスタックではwercker.ymlにスクリプトとしてビルド手順を記載する必要があります。

#### 移行前

移行前のwercker.ymlとDockerfileは以下のようになっていました。

```yaml
box: wercker-labs/docker
build:
  steps:
    - script:
        name: Build a CentOS 6.7 Docker Hatohol image box
        code: |
          docker -v
          echo ${WERCKER_GIT_COMMIT} > wercker/GIT_REVISION
          docker build -t centos67/build-hatohol wercker/
```


```dockerfile
from centos:6.7

maintainer hatohol project

# install libraries for Hatohol
RUN yum install -y glib2-devel libsoup-devel sqlite-devel mysql-devel mysql-server \
  libuuid-devel qpid-cpp-client-devel MySQL-python httpd mod_wsgi python-argparse
# setup mysql
RUN echo "NETWORKING=yes" > /etc/sysconfig/network
RUN rpm -ivh --force http://dl.fedoraproject.org/pub/epel/6/x86_64/epel-release-6-8.noarch.rpm
RUN yum install -y librabbitmq-devel wget
RUN wget -P /etc/yum.repos.d/ http://project-hatohol.github.io/repo/hatohol-el6.repo
# setup qpid
RUN yum install -y json-glib-devel qpid-cpp-server-devel
# setup build environment
RUN yum groupinstall -y 'Development tools'
# setup newer gcc toolchain
RUN cd /etc/yum.repos.d && wget http://people.centos.org/tru/devtools-2/devtools-2.repo
RUN yum install -y devtoolset-2-gcc devtoolset-2-binutils devtoolset-2-gcc-c++
RUN yum install -y Django
RUN rpm -Uvh http://sourceforge.net/projects/cutter/files/centos/cutter-release-1.1.0-0.noarch.rpm
RUN yum install -y cutter tar
# git clone
RUN git clone https://github.com/project-hatohol/hatohol.git ~/hatohol
# set CI terget to WERCKER_GIT_COMMIT
ADD ./GIT_REVISION /root/hatohol/wercker/
# change work dir
WORKDIR /root/hatohol
# checkout target branch
RUN git fetch origin
RUN git checkout -qf `cat ~/hatohol/wercker/GIT_REVISION`
# build
RUN libtoolize && autoreconf -i
RUN scl enable devtoolset-2 "./configure"
RUN scl enable devtoolset-2 "make -j `cat /proc/cpuinfo | grep processor | wc -l`"
RUN make dist -j `cat /proc/cpuinfo | grep processor | wc -l`
# rpm build
RUN yum install -y rpm-build
RUN scl enable devtoolset-2 "MAKEFLAGS=\"-j `cat /proc/cpuinfo | grep processor | wc -l`\" rpmbuild -tb hatohol-*.tar.bz2"
RUN scl enable devtoolset-2 "gcc --version"
```


また、Dockerfileとwercker.ymlの中で環境が異なるため、Werckerの環境変数(WERCKER_GIT_COMMIT)を用いてGitのコミットハッシュを指定したい場合、一旦ファイルに書き込んでからdocker build時に必要になる前にADDしてやる必要がありました。

#### 移行後

移行後のwercker.ymlは次のようになりました。

```yaml
box: centos:6.7
build:
  steps:
    - script:
        name: Install libraries for Hatohol
        code: |
          yum install -y glib2-devel libsoup-devel sqlite-devel mysql-devel mysql-server libuuid-devel qpid-cpp-client-devel MySQL-python httpd mod_wsgi python-argparse
    - script:
        name: Setup network
        code: |
          echo "NETWORKING=yes" > /etc/sysconfig/network

    - script:
        name: Setup yum repositories
        code: |
          rpm -ivh --force http://dl.fedoraproject.org/pub/epel/6/x86_64/epel-release-6-8.noarch.rpm
          yum install -y librabbitmq-devel wget
          wget -P /etc/yum.repos.d/ http://project-hatohol.github.io/repo/hatohol-el6.repo

    - script:
        name: Setup Qpid
        code: |
          yum install -y json-glib-devel qpid-cpp-server-devel

    - script:
        name: Setup build enviroment
        code: |
          yum groupinstall -y 'Development tools'

    - script:
        name: Setup newer gcc toolchain and libraries
        code: |
          cd /etc/yum.repos.d && wget http://people.centos.org/tru/devtools-2/devtools-2.repo
          yum install -y devtoolset-2-gcc devtoolset-2-binutils devtoolset-2-gcc-c++
          yum install -y Django
          rpm -Uvh http://sourceforge.net/projects/cutter/files/centos/cutter-release-1.1.0-0.noarch.rpm
          yum install -y cutter tar

    - script:
        name: Clone & fetch Hatohol repository
        code: |
          git clone https://github.com/project-hatohol/hatohol.git ~/hatohol
          cd ~/hatohol
          git fetch origin
          # HEAD is used for local `wercker` CLI command build
          git checkout -qf ${WERCKER_GIT_COMMIT:-HEAD}

    - script:
        name: Building Hatohol
        code: |
          cd ~/hatohol
          libtoolize && autoreconf -i
          scl enable devtoolset-2 "./configure"
          scl enable devtoolset-2 "make -j `cat /proc/cpuinfo | grep processor | wc -l`"
          make dist -j `cat /proc/cpuinfo | grep processor | wc -l`

    - script:
        name: Build RPM
        code: |
          yum install -y rpm-build
          cd ~/hatohol
          scl enable devtoolset-2 "MAKEFLAGS=\"-j `cat /proc/cpuinfo | grep processor | wc -l`\" rpmbuild -tb hatohol-*.tar.bz2"
          scl enable devtoolset-2 "gcc --version"
```


比べてみると、Dockerfile特有の記述ではなく、一般的なシェルスクリプトでビルド手順が記述出来るようになっていることが分かります。また、複数のステップに分割出来るようになったため、どのステップで失敗したかが分かりやすくなりました。

更に、wercker.ymlとシェルスクリプトの知識だけで完結するようになったため、Dockerを知らない人が見てもbox名の指定の注意点さえ抑えればメンテナンスが出来るようになりました。

この移行作業により、Werckerでのビルド時間が15分台から10分台に短縮されました。wercker-labs/dockerを用いてdockerを動かしている時にはWerckerの中でCI環境を構築している時に失敗していた現象が発生しなくなりました。

### まとめ

WerckerでDockerベースのビルドスタックに移行し、DockerHubからCentOSのDockerイメージを取ってきてCentOS向けのCIを行う事例を紹介しました。

新しいWerckerのビルドスタックではより手軽にDockerイメージをCIに用いることが出来るようになりました。また、ビルドに用いる環境変数をステップに組み込むことも容易になりました。

これを機に新しいWerckerのDockerベースのビルドスタックでのCIに挑戦してみるのはいかがでしょうか。
