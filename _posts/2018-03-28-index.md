---
tags:
- ruby
title: GObject Introspectionを使ったRubyバインディングの開発方法
---
日本ではだいぶ[GObject Introspection](https://wiki.gnome.org/Projects/GObjectIntrospection)に詳しい方だと思っている須藤です。
<!--more-->


バインディングの開発には5年くらい前からバインディングの開発にGOject Introspectionが有用だと思っています。

2013年には[各種バインディング開発方法についてまとめたり]({% post_url 2013-12-09-index %})、[GObject Introspection対応ライブラリーの開発方法を導入部分だけ説明したり]({% post_url 2013-12-16-index %})しました。

2016年には[RubyKaigi 2016で各種バインディング開発方法を紹介]({% post_url 2016-09-14-index %})しました。

2017年には[名古屋Ruby会議03でGObject Introspectionを使ったバインディングの開発方法のRubyレベルの部分だけを紹介]({% post_url 2017-02-15-index %})しました。

そして今年、1からGObject Introspection対応ライブラリーを開発する方法を1つずつ説明する文書をまとめました！[OpenCV](https://opencv.org/)をGObject Introspectionに対応させています。GObject Introspection対応ライブラリーを開発するための日本語の文書としては一番よい文書になっているはずです。

  * [GObject Introspection入門](https://github.com/RubyData/workshop-materials/blob/master/gobject-introspection/introduction.md)

この文書は[RubyData](http://ruby-data.org/)のリポジトリーで管理しています。RubyDataというのは[Speee](https://speee.jp/)の[@mrkn](https://twitter.com/mrkn)さんが始めた取り組みです。Ruby用のデータ処理ツールを開発する人たちとそのツールを使う人たちを増やすことを目指しています。

これまで、[RubyKaigi 2017でワークショップを開催](http://tech.speee.jp/entry/2017/09/17/005431)したり、サイトで関連情報をまとめたりしていました。RubyKaigi 2018でもワークショップを開催する予定です。

Rubyで使えるデータ処理関連のライブラリーが増えるとRubyでできることが増えます。バインディングの開発はデータ処理関連のライブラリーを増やす1つのやり方です。GObject Introspectionが有用なケースもあるはずです。ぜひ、この文書を活用してRubyで使えるデータ処理関連のライブラリーを増やしていきましょう。

興味のある人は[Red Data Tools](https://red-data-tools.github.io/ja/)の[チャットルーム](https://gitter.im/red-data-tools/ja)（オンライン）や[東京で開催している開発イベント](https://speee.connpass.com/event/82863/)（オフライン）にどうぞ！
