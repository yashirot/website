---
title: '#RedmineJapan で「今日から参加できる！OSS開発」という講演を行いました'
author: piro_or
tags:
- presentation
- redmine
---

結城です。

去る2023年7月7日に開催された[REDMINE JAPAN vol.3](https://redmine-japan.org/vol-3/)において、招待講演枠で「今日から参加できる！OSS開発」と題した講演を行いました。
当日の発表内容を改めてご紹介します。

<!--more-->

<div class="rabbit-slide">
  <iframe src="https://slide.rabbit-shocker.org/authors/Piro/presentation-redmine-japan-vol3-oss-gate/viewer.html"
          width="640" height="524"
          frameborder="0"
          marginwidth="0"
          marginheight="0"
          scrolling="no"
          style="border: 1px solid #ccc; border-width: 1px 1px 0; margin-bottom: 5px"
          allowfullscreen> </iframe>
  <div style="margin-bottom: 5px">
    <a href="https://slide.rabbit-shocker.org/authors/Piro/presentation-redmine-japan-vol3-oss-gate/" title="今日から参加できる！OSS開発">今日から参加できる！OSS開発</a>
  </div>
</div>

* [スライド(Rabbit Slide Show)](https://slide.rabbit-shocker.org/authors/Piro/presentation-redmine-japan-vol3-oss-gate/)
* [リポジトリー](https://gitlab.com/clear-code/presentation-redmine-japan-vol3-oss-gate)

REDMINE JAPANは基本的には、プロジェクト管理ツールのRedmineに関心のある方々向けのイベントですが、今回はイベント参加者をRedmineユーザー以外の層にも広げるための試みとして、基調講演をはじめとしていくつかの枠で、Redmineに限らずIT・ソフトウェアエンジニア向けの話題がセッティングされていた模様です。
筆者の発表もその中の1つで、Redmineには特に特化せず、一般的に「オープンソースの開発プロジェクトに関わってみよう」という事にフォーカスして話す構成としてみました。

2015年、当社代表の須藤が発起人となり[OSS Gate](http://oss-gate.github.io/)という取り組みを始めました。
OSS Gateは「OSS開発に参加する人を増やす」事を目的にしており、その名前は「OSS開発に参加した事がない人達が感じている、OSS開発をしている人達の世界との間の障壁に、穴を空けて入り口を作る」ことに由来しています。
「実際のオープンソース開発プロジェクトへ、issueを登録する・プルリクエストを作成するといった初めてのフィードバック体験をしてみよう」という趣旨のイベントである[OSS Gateワークショップ](https://oss-gate.github.io/workshop/report.html)は、2023年9月9日開催分までで計76回の開催実績があります。
また、ワークショップ内でのサポート経験に基づき、多くのオープンソース開発未体験者の方が不安や疑問に思っていることに答える書籍として、[これでできる！ はじめてのOSSフィードバックガイド](https://github.com/oss-gate/first-feedback-guidebook)も執筆・公開[^public]しています。

[^public]: 原稿状態であれば[無料でどなたでも読めます](https://github.com/oss-gate/first-feedback-guidebook/blob/master/index.md)。また、お手元に物理媒体で置いておきたい方向けの[ビルド済みPDF/ePUBファイル](https://sysadgirl.booth.pm/items/1842855)や、[オンデマンド印刷の紙媒体版](https://www.amazon.co.jp/dp/4844378813/)もご購入いただけます。

今回の発表の内容は、[OSS Gate ワークショップ](https://oss-gate.github.io/workshop/report.html)で前振りとして話している内容を膨らませたものです。
ワークショップでのサポート経験から、「オープンソース開発プロジェクトに関わっている人々は、自分とは縁遠い世界に生きているのだ」という意識を持たれているケースが多いように感じたので、「そんなことないですよ、普通の人もたくさん関わってますよ！」「スーパーエンジニアじゃなくても、スーパーエンジニアじゃないからこそできる・必要とされている関わり方もありますよ！」ということを、なるべく平易な表現で多く語るようにしてみました。

このプレゼン資料を見て「そんなにハードルが高くなさそうだな」と感じた方は、ぜひオープンソース開発プロジェクトに関わってみてください。
一人でやれる自信が無い場合には、[ワークショップ](https://oss-gate.doorkeeper.jp/)で現役オープンソース開発者のサポートを受けながら、実際のオープンソース開発プロジェクトに不具合や要望を報告したり、修正内容を提案したりしてみることもできます。

また、当社では企業を対象として、オープンソース開発に関わるためのお手伝いを行っております（[Speee社の事例](https://tech.speee.jp/entry/2021/03/01/182057)）。
「企業としてオープンソース開発プロジェクトへの貢献をしていきたい」「単なるフリーライダーではなくコミュニティの一員になっていきたい」とお考えの企業のご担当者の方や、シニアエンジニアの方で、そのための社内教育や意識改革のサポートを必要とされている方がおられましたら、[お問い合わせフォーム]({% link contact/index.md %})よりぜひご連絡下さい。

