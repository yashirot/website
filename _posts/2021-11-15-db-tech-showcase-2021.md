---
title: 'db tech showcase 2021 - Apache Arrow Flight – ビッグデータ用高速データ転送フレームワーク #dbts2021'
author: kou
tags:
  - apache-arrow
  - presentation
---

[db tech showcase 2021](https://www.db-tech-showcase.com/2021/)の11月17日（明後日！） 15:30-16:15のセッションで「Apache Arrow Flight – ビッグデータ用高速データ転送フレームワーク」という話をする須藤です。まだ登録できるのでApache Arrow Flightに興味がある人はぜひこのセッションに参加してください！オンライン開催です！セッション中およびセッション後はチャットで私と質疑応答できます！

<!--more-->

<div class="rabbit-slide">
  <iframe src="https://slide.rabbit-shocker.org/authors/kou/db-tech-showcase-2021/viewer.html"
          width="640" height="404"
          frameborder="0"
          marginwidth="0"
          marginheight="0"
          scrolling="no"
          style="border: 1px solid #ccc; border-width: 1px 1px 0; box-sizing: content-box; margin-bottom: 5px"
          allowfullscreen> </iframe>
  <div style="margin-bottom: 5px">
    <a href="https://slide.rabbit-shocker.org/authors/kou/db-tech-showcase-2021/" title="Apache Arrow Flight – ビッグデータ用高速データ転送フレームワーク">Apache Arrow Flight – ビッグデータ用高速データ転送フレームワーク</a>
  </div>
</div>


関連リンク：

  * [スライド（Rabbit Slide Show）](https://slide.rabbit-shocker.org/authors/kou/db-tech-showcase-2021/)

  * [スライド（SlideShare）](https://www.slideshare.net/kou/db-tech-showcase-2021)

  * [リポジトリー](https://gitlab.com/ktou/rabbit-slide-kou-db-tech-showcase-2021)

### 内容

[db tech showcase ONLINE 2020ではApache Arrowフォーマットに焦点を当ててなぜ速いのかを紹介]({% post_url 2020-12-07-index %})しましたが、今年はApache Arrow Flightに焦点を当ています。

Apache Arrow FlightはApache Arrowフォーマットのデータ転送効率を活かしたデータ転送フレームワークです。Apache Arrow Flightを使うことで高速にデータを転送する分散データ分析システムを実装できます。

Apache Arrowフォーマットを使うことでシリアライズ・デシリアライズコストなしでデータ転送できる（詳細はdb tech showcase ONLINE 2020の資料を見てね）ことが速さの秘密の1つですが、それ以外にもデータ転送を高速にする仕組みが入っています。

たとえば並列転送です。Apache Arrow Flightは1つのデータセットに対して1つ以上のノードからデータをダウンロードできる仕組みになっています。複数のノードからデータを並列にダウンロードすればより効率的にデータを取得できます。

また、データをストリームで処理できる仕組みになっているので取得したデータを順次処理できます。並列転送機能と組み合わせることで大きなデータセットでも効率よく処理できます。

すでにApache Arrow Flightを使っているアプリケーションもあります。Apache Arrowプロジェクトで開発している[Apache Arrow Ballista](https://github.com/apache/arrow-datafusion/tree/master/ballista)もその1つです。Apache Arrow BallistaはApache Sparkのような分散計算プラットフォームです。

Apache Arrow Ballistaは基本的にgRPCを使って通信していますが、計算結果を取得するところだけはApache Arrow Flightを使っています。通常の通信（クエリーの送信や実行プランの送信など）は小さなデータですが、計算結果は大きなデータになりうるからです。大きなデータを転送するときはApache Arrow Flightの仕組みが活きるのです。

Apache Arrow Ballistaはパーティションされたデータを各ノードが並列に処理します。これは処理結果は各ノードが持っているということです。クライアントは処理結果を各ノードから取得します。「コーディネーター」のような「いい感じに取りまとめてくれるノード」経由で取得するわけではありません。各ノードから直接です。ここでApache Arrow Flightの並列転送機能が活きています。コーディネーターを経由しないのでムダなデータ転送がありません。

Apache Arrow Flightは今も積極的に開発が進んでいます。たとえば、Apache Arrow Flight SQLというApache Arrow Flight上でSQLを扱えるようにする機能の開発も進んでいます。（興味がある人は[ARROW-9825 \[FlightRPC\] Add a "Flight SQL" extension on top of FlightRPC](https://issues.apache.org/jira/browse/ARROW-9825)とかを見てね。）

と、こんな話をもっと詳しく紹介するのがdb tech showcase 2021での話です。興味が出てきた人は[db tech showcase 2021](https://www.db-tech-showcase.com/2021/)の11月17日（明後日！） 15:30-16:15の「Apache Arrow Flight – ビッグデータ用高速データ転送フレームワーク」を聞きに来てね！

聞きに来てくれた人からも資料だけを読んでくれた人からも感想を聞きたいので、[アンケート]({% link surveys/db-tech-showcase-2021.md %})に答えてくれるとうれしいな！

### まとめ

2021年11月17日（明後日！） 15:30-16:15にdb tech showcase 2021でApache Arrow Flightを紹介します。セッション中は私と質疑応答できるので、興味がある人はぜひセッションに参加してください！

都合があわないという人はすでにスライドを公開しているのでそれを参照してください。後日動画も公開する予定です！感想・質問は https://twitter.com/ktou でお待ちしています！[アンケート]({% link surveys/db-tech-showcase-2021.md %})での回答でも大丈夫です！

Apache Arrow関連の技術サポートが必要な場合は[Apache Arrowサポートサービス]({% link services/apache-arrow.md %})をごらんください。
