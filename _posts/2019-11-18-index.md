---
tags:
- groonga
- presentation
title: 'PostgreSQL Conference Japan 2019：Amazon RDS + Amazon EC2 + ロジカルレプリケーションを使った低コスト高速全文検索
  #pgcon19j'
---
2019年11月15日(金)に[PostgreSQL Conference Japan 2019](https://www.postgresql.jp/jpug-pgcon2019)が開催されました。
私は、「Amazon RDS + Amazon EC2 + ロジカルレプリケーションを使った低コスト高速全文検索」という題名で、既存の技術を組み合わせて、なるべく楽に高速、高機能な全文検索ができる仕組みを紹介しました。
<!--more-->


当日、使用した資料は以下に公開しています。

<div class="rabbit-slide">
  <iframe src="https://slide.rabbit-shocker.org/authors/komainu8/postgresql-conference-japan-2019/viewer.html"
          width="640" height="510"
          frameborder="0"
          marginwidth="0"
          marginheight="0"
          scrolling="no"
          style="border: 1px solid #ccc; border-width: 1px 1px 0; margin-bottom: 5px"
          allowfullscreen> </iframe>
  <div style="margin-bottom: 5px">
    <a href="https://slide.rabbit-shocker.org/authors/komainu8/postgresql-conference-japan-2019/" title="Amazon RDS + Amazon EC2 + ロジカルレプリケーションを使った低コスト高速全文検索">Amazon RDS + Amazon EC2 + ロジカルレプリケーションを使った低コスト高速全文検索</a>
  </div>
</div>


関連リンク：

  * [スライド(Rabbit Slide Show)](https://slide.rabbit-shocker.org/authors/komainu8/postgresql-conference-japan-2019/)

  * [リポジトリー](https://github.com/komainu8/rabbit-slide-komainu8-postgresql-conference-japan-2019)

### 内容

RDBMSのマネージドサービスにAmazon RDSがあります。Amazon RDSはマネージドサービスなので、RDBMSの管理や運用などのコストを大幅に下げるような機能(自動レプリケーション、自動フェイルオーバー、自動バックアップ、設定の最適化などなど)がたくさんあり、これらを使うことで、DBの管理、運用はとても楽になります。もちろん、PostgreSQLにも対応しておりPostgreSQLの管理、運用も大幅に楽になります。

Amazon RDSはとても便利なのですが、Amazon RDS上のPostgreSQLには決められた拡張しかインストールできません。
したがって、Amazon RDS上のPostgreSQLで全文検索をしたい場合、pg_trgmを使った全文検索をすることになります。(pg_trgmはAmazon RDS上で使用できます)
しかしながら、pg_trgmはアルファベットと数字にしか対応していません。日本語や中国語などの全文検索には使用できません。

PostgreSQLで使用できる全言語対応の全文検索の拡張には、[PGroonga(ぴーじーるんが)](https://pgroonga.github.io/ja/)があります。
PGroongaは、全言語対応の超高速全文検索機能をPostgreSQLで使えるようにする拡張で、安定して高速で、かつ高機能（同義語、表記ゆれや異体字への対応、類似文書検索など）ですが、前述の通り、Amazon RDS上では使用できません。

Amazon RDS上のPostgreSQLには直接PGroongaをインストールできないので、PostgreSQL10から追加されたロジカルレプリケーションと、Amazon EC2を使用してAmazon RDSのメリットである、管理、運用コストの低減を享受しつつ、高速、高機能な全文検索を実現する構成を紹介しました。

PostgreSQLでは、ストリーミングレプリケーションというレプリケーションがありますが、これは、複製元と複製先のDBはまったく同じものになります。
一方、ロジカルレプリケーションでは、複製元と複製先でテーブルの構造を一致させなくても良いという特徴があります。
この特徴を利用すると、複製先のテーブルにのみインデックスを設定するということができます。

つまり、複製元をAmazon RDSとし、複製先にはAmazon EC2を用意します。Amazon EC2にはPostgreSQLとPGroongaをインストールしておき、更新はAmazon RDS、検索はAmanzon EC2で行うという構成を取ることができます。

この構成では、Amazon RDSにさえデータがあれば、検索用のEC2はいくらでも作れるという特徴があります。
そのため、Amazon RDSのデータは大事に保護しなければなりませんが、DBの管理、運用に便利な機能があるAmazon RDSを使っているので、データの保護は今までよりも大幅に楽にできます。

一方で、Amazon EC2では、データをAmazon RDSからロジカルレプリケーションで同期するため、PGroongaを使ってAmazon RDSにあるデータを高速に全文検索できます。

このように、ロジカルレプリケーションの複製元と複製先のテーブル構造を一致させなくても良いという特徴を使って、Amazon RDSのメリットを享受しつつ、高速で高機能な全文検索を実現できるのです。

### まとめ

Amazon RDSのメリットを活かしつつ、PGroongaを使った高速で高機能な全文検索を実現する構成を紹介しました。
Amazon RDSを使いたいけど、PGroongaが使えないので、しかたなく自前でサーバーを用意している。という方や、PGroongaを使って、高速で高機能な全文検索をしたいけど、Amazon RDSを使っているのでPGroongaが使えないとお悩みの方は、ここで紹介した構成を検討してみてはいかがでしょうか？

PGroongaを使った全文検索について、興味、疑問などございましたら、是非、[お問い合わせ](/contact/)ください。
