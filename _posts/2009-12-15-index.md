---
tags:
- ruby
title: 'Ruby on Rails Technical Night: Railsで作るActive Directoryと連携した社内システム'
---
先日開催された[〜Ruby on Rails Technical Night〜 Ruby on Railsセミナー](http://www.contents-one.co.jp/ruby/2009/11/ruby-on-railsruby-technical-night.php)でActive Directoryと連携したRailsアプリケーションの作り方について話しました。
<!--more-->


[![Railsで作るActive Directoryと連携した社内システム]({{ "/images/blog/20091215_0.png" | relative_url }} "Railsで作るActive Directoryと連携した社内システム")](/archives/rails-seminar-technical-night/)

### 概要

ActiveLdapという社内システムをRailsを使って実現するときに便利なライブラリをデモを交えながら紹介しました。

社内向けのシステムをWebアプリケーションとして実現することは驚くことではなくなりました。Webアプリケーションなので、もちろんRailsを使っても実現することができます。

そのときに避けて通れないのが既存の社内情報との連携です。社内向けのシステムなので、社内情報と密接に連携し、より便利に使えるものであるべきです。多くの組織では社内情報をActive Directoryを用いて一元管理しています。

Railsアプリケーションとして社内システムを実現する場合も、必要に応じてActive Directory内にある情報を利用します。それを助けてくれるライブラリがActiveLdapです。

### 内容

Active Directoryをあまり知らない方も参加されるかたでもついてこられるように[^0]、前半でActive DirectoryとLDAPの基本的なところを説明しました。今回は「図での説明 + まとめ」というように、感覚的にわかってもらった後（なんとなくわかってもらった後）に要点を確認するという流れにしました。参加された方に感想を聞けなかったのですが、いかがだったでしょうか。

その後、実際にデモを行いながら、ActiveLdapがActiveRecordと同じようにActive Directory上の情報を操作できることを説明しました。ActiveRecordと同じように操作できるということは、いつもと同じようにコントローラ部分を書けるということです。つまり、今までのRailsアプリケーション開発の知識を活かしながらActive Directoryを操作するRailsアプリケーションを開発することができるということです。

今回は「コードがバンバン出るような内容を」ということで声をかけてもらったので、ここがメインの内容になっています。残念ながら公開されている資料ではデモを表現することができないので、デモで実行したコマンドなどを載せています。

最後に、実際にアプリケーションを開発するときにぶつかることが多い問題点についてふれました。Active Directoryとの接続の仕方、テストの仕方などです。

### まとめ

先日開催されたRailsセミナーでの内容を紹介しました。

ActiveLdapに興味をもたれた方は[るびまのActiveLdap を使ってみよう（前編）](http://jp.rubyist.net/magazine/?0027-ActiveLdap)や[ActiveLdapのチュートリアル](http://code.google.com/p/ruby-activeldap/wiki/TutorialJa)も読んでみてください。次号のるびまでは後編が公開される予定です。そちらも楽しみですね。

今回の内容はActive Directoryに特化した内容もありますが、OpenLDAPなどLDAPサーバ一般に通用する内容も多いです。LDAPサーバと連携するRailsアプリケーションを開発している方も参考にしてみてください。

Active Directory関連のことだけではなくて、[コードを書くこと](http://www.clear-code.com/archives/rails-seminar-technical-night/rails-with-active-directory-23.html)についても伝えたかったのですが、欲張りすぎました。話の中ではうまく伝えられませんでしたが、プログラマーの方であれば、[グラデーションで繋がる世界: 札幌Ruby会議02に行ってみて初心に帰った](http://adzuki34.blogspot.com/2009/12/ruby02.html)もぜひ読んでみてください。

[^0]: 多くの方はご存知のようでしたが。。。
