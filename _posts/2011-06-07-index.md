---
tags:
- ruby
title: 日本Ruby会議2011でるりまとテスティングフレームワークについて話します
---
[日本Ruby会議2011のスケジュール](http://rubykaigi.org/2011/ja/schedule/grid)が発表されました。クリアコードのメンバーは[るりま](http://rubykaigi.org/2011/ja/schedule/details/18M01)に関することと[テスティングフレームワーク](http://rubykaigi.org/2011/ja/schedule/details/18M06)に関することを話します。面白そうだなと思ったら聞きに来てください。
<!--more-->
