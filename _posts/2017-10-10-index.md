---
tags:
- groonga
- presentation
title: 'PHPカンファレンス2017 - PostgreSQLとPGroongaで作るPHPマニュアル高速全文検索システム - OSS Gate東京ワークショップ
  #phpcon2017'
---
はじめてPHPカンファレンスに参加した須藤です。午前に「PostgreSQLとPGroongaで作るPHPマニュアル高速全文検索システム」という話をして、午後に「[OSS Gate東京ワークショップ2017-10-08 - PHPカンファレンス2017会場編 -](https://phpcon.connpass.com/event/66822/)」を開催しました。
<!--more-->


「PostgreSQLとPGroongaで作るPHPマニュアル高速全文検索システム」のスライドは次の通りです。発表後にオートコンプリートの候補を登録する方法を話し忘れたことに気づいたので追記しました。

<div class="rabbit-slide">
  <iframe src="https://slide.rabbit-shocker.org/authors/kou/php-conference-2017/viewer.html"
          width="640" height="524"
          frameborder="0"
          marginwidth="0"
          marginheight="0"
          scrolling="no"
          style="border: 1px solid #ccc; border-width: 1px 1px 0; margin-bottom: 5px"
          allowfullscreen> </iframe>
  <div style="margin-bottom: 5px">
    <a href="https://slide.rabbit-shocker.org/authors/kou/php-conference-2017/" title="PostgreSQLとPGroongaで作るPHPマニュアル高速全文検索システム">PostgreSQLとPGroongaで作るPHPマニュアル高速全文検索システム</a>
  </div>
</div>


動画もあります。

<div class="youtube">
  <iframe width="560" height="315" src="https://www.youtube.com/embed/8qFKHEhZXm8?rel=0" frameborder="0" allowfullscreen></iframe>
</div>


関連リンク：

  * [スライド（Rabbit Slide Show）](https://slide.rabbit-shocker.org/authors/kou/php-conference-2017/)

  * [スライド（SlideShare）](https://www.slideshare.net/kou/phpconference2017)

  * [動画（YouTube）](https://www.youtube.com/watch?v=8qFKHEhZXm8)

  * [リポジトリー](https://github.com/kou/rabbit-slide-kou-php-conference-2017)

### 背景

[第115回 PHP勉強会＠東京](https://phpstudy.doorkeeper.jp/events/61730)で[PGroongaのことを話す機会があった]({% post_url 2017-07-20-index %})ので、そのデモのためにPHPのマニュアルをPGroongaで全文検索するアプリケーションを作りました。作ってみたところ、PHPユーザーにも便利そうな気がしてきたので、使ってもらったり開発に参加してもらうとよさそうだなぁと思い、PHPカンファレンス2017でも紹介しました。後述するOSS Gateワークショップの開催も第115回 PHP勉強会＠東京のときに相談した結果として実現できたので、とてもよい機会でした。

今回の発表では、第115回 PHP勉強会＠東京での内容に次の内容を加えました。

  * 類似マニュアル検索の実現方法

  * 同義語展開の実現方法

今回の発表の成果は悪くないです。この[PHPマニュアル検索システム](https://github.com/kou/php-document-search)の開発に参加する人は増えていないのですが、この検索システムを運用してもらえることになりました！

PHPユーザーで、（PGroongaではなく）Mroongaユーザーでもある[WEIC](http://weic.co.jp/)さんが運営することになりました！今月中の運用開始を目指しています。URLは以下になる予定です。

  * [http://phpdocs.weic.co.jp](http://phpdocs.weic.co.jp)

運用が始まったら、おそらく[WEICさんのお知らせページ](http://weic.co.jp/news/)で告知されるので、チェックしてください！

簡単に使えるようになったらユーザーも開発者も増えることを期待しています。

そうそう、[WEICさんはPHPエンジニアを大募集中](https://jp.stanby.com/ats/862502031087124481/jobs)だそうです！

### OSS Gateワークショップ

午後は2時間45分の枠をもらって[OSS Gateワークショップ](https://github.com/oss-gate/workshop/tree/master/tutorial)を開催しました。（終了時間を勘違いしていて25分早く終わらせてしまいました。。。）

このワークショップには通常バージョンとショートバージョンがあり、通常バージョンは5,6時間、ショートバージョンは2,3時間です。今回は3時間弱の枠だったのでショートバージョンで開催しました。

ショートバージョンは過去に[OSS Gate東京ワークショップ for Mastodon in ピクシブ 2017-06-29](https://oss-gate.doorkeeper.jp/events/61807)で実施していて、今回もそれと同じように進めました。通常バージョンからの差は次の通りです。

  * できるだけ事前に対象OSSを決める

  * ミニふりかえりなし

    * 進行役・サポートメンターが随時まわってミニふりかえり的なことをしていく

  * 「ユーザーとして動かす」と「フィードバック」の時間を一緒にする

    * フィードバックポイントが見つかったらすぐにフィードバックに取り組む

今回、私が終了時間を勘違いしていたため最後のアンケートとふりかえりも省略しました。アンケートとふりかえりをやる時間はあったので失敗でした。

今回は時間内にフィードバックまでいけた人はほとんどいませんでした。フィードバックポイントに気づくところまではほとんどの人が経験していたので、フィードバックをまとめる時間が足りなかったということになります。予想した要因は次の通りです。

  * サポーター1人でビギナー2人だとサポートが手薄

    * ピクシブさんで開催したときはサポーター1人でビギナー1人だった

    * ビギナーの人がもう少しサポートを厚くできると時間を短くできそう

試してみたいことは次の通りです。

  * サポーター2人でビギナー3人をサポート

  * サポーター1人でビギナー2人をサポートするけど、サポーター2人つきサポートメンターが1人サポート

    * 今回はサポーター4人につきサポートメンターが1人サポート

今回、初めてPHP関連のOSS限定のOSS Gateワークショップを開催しました。今回だけで終わりにせずに今回が始まりにしていきたいので、PHP関連のOSSの開発に参加する人が増えるとうれしいという人はぜひ一緒にやっていきましょう！[Gitterのoss-gate/tokyo-php](https://gitter.im/oss-gate/tokyo-php)でオンラインで相談しています。まずはここに来てください！現状は、今回ワークショップに参加した人と次回の開催について相談するところです。

### まとめ

PHPカンファレンス2017で、PGroongaの話をして、OSS Gateワークショップも開催しました。今回の機会をぜひ今後につなげていきたいです。
