---
tags:
- groonga
- presentation
title: '第一回 JPMUG DB勉強会 - MariaDBとMroongaで作る全言語対応超高速全文検索システム #JPMUG'
---
1ヶ月半前の話をようやくまとめている須藤です。
<!--more-->


1月30日に[第一回 JPMUG DB勉強会](https://jpmug.connpass.com/event/76952/)でMroongaの使い方を紹介してきました。

<div class="rabbit-slide">
  <iframe src="https://slide.rabbit-shocker.org/authors/kou/jpmug-db-study-1/viewer.html"
          width="640" height="524"
          frameborder="0"
          marginwidth="0"
          marginheight="0"
          scrolling="no"
          style="border: 1px solid #ccc; border-width: 1px 1px 0; margin-bottom: 5px"
          allowfullscreen> </iframe>
  <div style="margin-bottom: 5px">
    <a href="https://slide.rabbit-shocker.org/authors/kou/jpmug-db-study-1/" title="MariaDBとMroongaで作る全言語対応超高速全文検索システム">MariaDBとMroongaで作る全言語対応超高速全文検索システム</a>
  </div>
</div>


関連リンク：

  * [スライド（Rabbit Slide Show）](https://slide.rabbit-shocker.org/authors/kou/jpmug-db-study-1/)

  * [スライド（SlideShare）](https://slideshare.net/kou/jpmugdbstudy1)

  * [リポジトリー](https://github.com/kou/rabbit-slide-kou-jpmug-db-study-1)

### 内容

対象は全文検索システムを作ったことがないMariaDBユーザーです。そのため、「全文検索システム」にはどんな機能が必要なのか、それを実現するには具体的にどんなSQLを書くことになるのか、ということをまとめた内容になっています。

また、事前に「日本語以外の言語にも対応するにはどうすればいいの？」という質問をもらっていたので、MariaDB・MroongaでのCOLLATION関連の話やUnicodeの話も少し含めています。

### まとめ

第一回 JPMUG DB勉強会でMariaDBとMroongaを使って全文検索システムを実現する方法を説明しました。ぜひ、この資料を参考に全文検索システムを実現してみてください。

困ったことがあり、有償サポートが必要な場合は、[お問い合わせ](/contact?type=groonga)ください。
