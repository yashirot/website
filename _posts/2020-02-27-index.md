---
tags:
- mozilla
title: Firefoxでデフォルト有効化されるかもしれないDNS over HTTPSへの企業での対応について
---
昨日、Firefoxの米国エリアユーザーに対して段階的に「DNS over HTTPS」の有効化が反映されていくことになった、という話がニュースになっていました。
<!--more-->


  * [ついにFirefoxがDNSとの通信を暗号化する「DNS over HTTPS」をデフォルトで有効にすると発表 - GIGAZINE](https://gigazine.net/news/20200226-firefox-dns-over-https-default-us/)

### DNS over HTTPSとは？

#### 今までの名前解決との違い

DNSとは、`www.clear-code.com`のようなドメイン名の文字列を`153.126.132.63`のようなIPアドレスに解決する（名前解決をする）という、インターネットにおけるかなり根幹の部分を支えている仕組みです。

この問い合わせ・応答の通信は特に暗号化も認証もなされていないため、問い合わせ内容の盗聴や応答の偽装が技術的には可能となっています。身近な場面では、空港や飲食店などのパブリックWi-Fiでそのような攻撃を受ける恐れがあるほか、地域によっては政府主導で盗聴・検閲が行われているケースもあるといいます。DNS over HTTPSは、このような脅威からユーザーを守る技術であるとされています。

ただし「DNS over HTTPSであれば何もかもが安全になる」というわけではないことに注意が必要です。DNS over HTTPSで安全になるのは全体の中のごく一部のやり取りのみで、それ以外の部分については別の技術的手段での保護が依然として必要です。また、組織内のみのドメインが名前解決できなくなるようなトラブルや、CDN[^0]の性能が発揮されないといった問題が起こる場合もあります。

具体的にDNS over HTTPSによって何が守られて何が守られないのかについては、ISP[^1]であるIIJの技術者向けブログの以下の記事に詳しい解説がありますので、ぜひご参照下さい。

  * [DNS over TLS/HTTPSについて考える | IIJ Engineers Blog](https://eng-blog.iij.ad.jp/archives/2954)

#### FirefoxでのDNS over HTTPS

Firefoxはセキュリティやプライバシーの保護を理由として、DNS over HTTPSを推進していくことを以前から発表しています。今回行われたのは「米国地域内のユーザーに対して、DNS over HTTPSをデフォルト有効にする」という変更でした。

  * 問答無用で機能が使われるというわけではなく、初めて機能が有効化されるときには確認のポップアップが表示されるほか、DNS over HTTPSでの名前解決に失敗した場合などには通常の名前解決の仕組みにフォールバックする、といった動作になっています。

  * DNS over HTTPSの機能を提供するプロバイダは、初期状態ではCloudflareとNextDNSという2つのプロバイダが選択可能になっているほか、[curlのWikiに記載されているリスト](https://github.com/curl/curl/wiki/DNS-over-HTTPS#publicly-available-servers)にあるような、DNS over HTTPS対応の任意のプロバイダを使うこともできます。

    * 既定のプロバイダがこの2者である理由は、[<q>機密情報の保持を定める「データの制限」、ポリシーの明文化を約束する「透明性」、プロバイダー側が検閲などを行えないようにする「遮断と修正」の3つをDNSプロバイダーに要求するプロトコル・TRRプログラム</q>に参加していること](https://gigazine.net/news/20191218-firefox-new-partner-nextdns/)によります。他のプロバイダの状況については、[Mozillaの社内情報共有サイト上にある、TRRプログラムに参加しているプロバイダの一覧](https://wiki.mozilla.org/Security/DOH-resolver-policy#Conforming_Resolvers)を参照するとよいでしょう。

  * 米国以外の地域でこの機能がデフォルト有効化されるかどうかは、現時点では未定とのことです。

Firefox上でDNS over HTTPSを使用する場合の注意点・無効化の方法などは、以下の公式のサポート情報に記載があります。

  * [Firefox DNS-over-HTTPS | Firefox ヘルプ](https://support.mozilla.org/ja/kb/firefox-dns-over-https)

### 企業運用においてFirefoxのDNS over HTTPSを強制的に無効化するには

Firefoxを企業内で運用する場合、現時点で直接的に影響を受けるのは米国地域内の拠点だけということになります。しかし、今後DNS over HTTPSが全世界で有効化されていく可能性はありますし、また、現時点でもユーザーが任意に機能を有効化できます。使われると企業内のセキュリティポリシーを守れない、などのさまざまな理由から、社内ではDNS over HTTPSを使わせたくないということもあるでしょう。

そのような場合、以下の方法で*DNS over HTTPSを無効に固定する*（ユーザーが任意に有効化できなくする）ことができます。

#### Windows Server 2008以降のバージョンでActive Directoryによるドメイン管理が行われている場合

  1. [.admx形式のグループポリシーテンプレートと言語リソース](https://github.com/mozilla/policy-templates/releases)をダウンロードする。ここでは `policy_templates_v1.13` をダウンロードしたと仮定する。

  1. ダウンロードしたファイルの名前を変更し、末尾に`.zip`を付ける。

  1. ファイルをZIPアーカイブとして展開し、`windows` フォルダ内のすべてのファイルを、ドメインコントローラの `C:\Windows\SYSVOL\domain\Policies\PolicyDefinitions` に設置する。

  1. グループポリシーの管理画面を開き[^2]、「コンピューターの構成」の「ポリシー」→「管理用テンプレート」→「Mozilla」→「Firefox」で「Configure DNS Over HTTPS」をダブルクリックする。

  1. ポリシーを「有効」にし、以下の通り設定する。

     * 「Enable DNS over HTTPS.」のチェックを外す。

     * 「Don't allow DNS over HTTPS preferences to be changed.」のチェックを入れる。

このように設定しておくことで、そのドメインに参加しているWindows端末上で動作するFirefoxでは常にDNS over HTTPSが無効となります。

#### macOSの場合

  1. 端末を開く。

  1. 以下の3つのコマンド列を順番に実行する。

     ```sh
     sudo defaults write /Library/Preferences/org.mozilla.firefox EnterprisePoliciesEnabled -bool TRUE 
     sudo defaults write /Library/Preferences/org.mozilla.firefox DNSOverHTTPS__Enabled -bool FALSE 
     sudo defaults write /Library/Preferences/org.mozilla.firefox DNSOverHTTPS__Locked -bool TRUE 
     ```


このように設定しておくことで、そのマシン上で動作するFirefoxでは常にDNS over HTTPSが無効となります。

#### それ以外の場合（Windows、Linux）

  1. Firefoxのインストール先（Firefoxの実行ファイルがあるフォルダ）に `distribution` という名前でフォルダを作成する。

  1. 以下の内容のプレーンテキストファイルを作成し、`policies.json`という名前で1のフォルダ内に設置する。

     ```json
     {
       "policies": {
         "DNSOverHTTPS": {
           "Enabled": false,
           "Locked": true
         }
       }
     }
     ```


このように設定しておくことで、その位置にインストールされているFirefoxでは常にDNS over HTTPSが無効となります。

この方法は、Active Directoryを運用していない場合や、シンクライアント端末からWindows Serverにリモートデスクトップ接続してFirefoxを使用する場合などに有用でしょう。

#### Canary Domainを利用する方法

ポリシー設定を使う方法とは別のやり方として、Canary Domainを使用する方法があります。具体的には、*`use-application-dns.net` について `NXDOMAIN` を返すようにそのネットワーク内のDNSを設定しておく*ことにより、Firefoxはそのことを検出し、自動的にDNS over HTTPSを使用しなくなります。

これについての詳しい解説は、以下の記事をご参照ください。

  * [DoHを利用しないように指示するCanary domainの仕組み - ASnoKaze blog](https://asnokaze.hatenablog.com/entry/2019/12/01/161259)

社外に持ち出して使用することがある端末（ラップトップPCなど）が存在していて、社内ではDNS over HTTPSを無効化したいが、社外ネットワークでの利用に関しては特に制御したくない、という場合には特にこの方法が有用でしょう。

### まとめ

DNS over HTTPSの概要と、ポリシー設定等を使って企業内でFirefoxのDNS over HTTPS動作を常に無効化する方法をご紹介しました。

当社では、お客さまからの技術的なご質問・ご依頼に有償にて対応する[Firefoxサポートサービス](/services/mozilla/menu.html)を提供しています。企業内でのFirefoxの運用でお困りの情シスご担当者さまやベンダーさまは、[お問い合わせフォーム](/contact/)よりお問い合わせください。

[^0]: Content Delivery Network。動画などのネットワーク転送負荷が高いデータについて、名前解決時にホストをネットワーク的な距離が近いホストに解決することで、ネットワーク負荷を下げたりデータ転送にかかる時間を短縮したりするための仕組み。

[^1]: Internet Service Provider。インターネット接続事業者。

[^2]: Windows Server 2008では「Active Directory ユーザーとコンピュータ」を開き、ドメイン名を右クリックし「プロパティ」をクリックして、「グループポリシー」タブで「編集」ボタンをクリックする。Windows Server 2012では「グループ ポリシーの管理」を開いて「Default Domain Policy」を右クリックし「編集」を選択する。
