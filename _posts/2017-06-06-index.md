---
tags:
- ruby
title: 'RubyKaigiのCFPへの応募例 #rubykaigi'
---
須藤です。[関西Ruby会議2017]({% post_url 2017-05-29-index %})が終わってからRubyKaigi 2017の発表を応募しました。
<!--more-->


[RubyKaigi 2017](http://rubykaigi.org/2017)の[CFPがでています](https://cfp.rubykaigi.org/events/2017)。CFPとはもともと（？学会の文脈で）はCall For Papersの略ですが、RubyKaigiの文脈ではCall For Proposalsの略で、「RubyKaigiでの発表を募集しています」という意味です。RubyKaigi 2017の発表の応募は6月17日まで受け付けています。

応募する人が増えるといいなぁと思うので、実際の応募例として私のRubyKaigi 2015からRubyKaigi 2017の分の応募内容を紹介します。どうしてRubyKaigi 2015の分からかというと、CFPアプリケーションができて応募の記録が残るようになったのがRubyKaigi 2015からだからです。

### 項目

実際の応募内容を紹介する前に、どのような項目を書く必要があるかを紹介します。

  * Title: 発表のタイトル（60文字以下。英語。）

  * Abstract: 発表の概要（600文字以下。英語。）

  * Details: 発表の詳細（<del>日本語可</del>日本語でも検討してもらえるが、RubyKaigiは国際カンファレンスなので英語が望ましい。）

  * Pitch: どうして自分がRubyKaigiでこの発表をするべきなのかの説明（<del>日本語可</del>日本語でも検討してもらえるが、RubyKaigiは国際カンファレンスなので英語が望ましい。）

TitleにはRubyKaigiに参加する人が「どの発表を聞くか」をパッと選ぶときに参考になりそうなものを書きます。

AbstractにはRubyKaigiに参加する人が「どの発表を聞くか」を検討するときに参考になりそうな説明を書きます。

Detailsには発表を選考する人が「どの発表を選ぶか」を検討するときに参考になりそうな発表の説明を書きます。発表を聞いた人が何を得られる発表なのかを書くとよいでしょう。

Pitchには発表を選考する人が「どの発表を選ぶか」を検討するときに参考になりそうな発表者の説明を書きます。たとえば、「私が実装した人なので私が一番詳しいです。なので、私が話すのが一番いいんです！」ということを書きます。

これらのことは応募フォームの説明にも書いているので、応募するときには説明をちゃんと読んで応募内容がずれていないか確認するとよいでしょう。

応募の書き方については以下の情報が参考になります。

  * [日本Ruby会議2011 CFP虎の巻: はじまりはじまり, 理解されるように書く, テーマを広げる, オリジナリティを明確にする, 発表者の立ち位置を明らかにする - RubyKaigi Nikki(2011-03-08)](http://rubykaigi.tdiary.net/20110308.html)

  * Proposal to RubyKaigi 2014 - Qiitaの[コメント](http://qiita.com/chihayafuru/items/0a32fd3c68661a5be4be#comment-622b5a3024bf6a0595c9)

明示的に書かれている情報をインターネット上に見つけられませんでしたが、RubyKaigiは「Ruby」の「Kaigi」なので、Rubyで作られたなにかの発表よりもRubyそのものの発表の方がRubyKaigiにあっていそうです。

### 応募例：RubyKaigi 2015

RubyKaigi 2015での応募例です。この応募は採択されました。

発表内容：[The history of testing framework in Ruby]({% post_url 2015-12-12-index %})

Title:

> The history of testing framework in Ruby


Abstract:

> This talk describes about the history of testing framework in Ruby.
> Ruby 2.2 bundles two testing frameworks. They are minitest and test-unit. Do you know why two testing frameworks are bundled? Do you know that test-unit was bundled and then removed from Ruby distribution? If you can't answer these questions and you're interested in testing framework, this talk will help you.
> This talk describes about the history of bundled testing framework in Ruby and some major testing frameworks for Ruby in chronological order.


Details:

> このトークは参加者が「自分にとって適切なテスティングフレームワークを選ぶための知識」を持ち帰れることを目標とします。
>
> テスティングフレームワークはRubyで開発する上で重要な役割を持つソフトウェアです。Rubyを使うと非常に柔軟にプログラムを書くことができるため、意図せずに既存の挙動を変えてしまうことが起こしやすいです。自動化されたテストも一緒に開発すると、できるだけ低いコストでその問題の発生を抑えつつ、楽しくプログラムを書きやすくなります。それを支援するのがテスティングフレームワークです。
>
> テスティングフレームワークはRubyで楽しくプログラムを書くために重要な役割にも関わらず、自分にとって適切なテスティングフレームワークを選ぶという活動はあまり行われていないように感じています。多くの人が使っているから、というのも選んだ理由としては妥当な理由ではあるのですが、流行が変わったとき・テスティングフレームワークのAPIが変わったときなど、今までと状況が変わったときに適切な対応を取りづらくなります。大きな流れが1つではなくなるからです。
>
> 自分で「どうしてこのテスティングフレームワークを選んでいるのか」を理解していれば、たとえ状況が変わったときでも適切な基準で適切なフレームワークを選びなおすことができるでしょう。
>
> このトークではRubyにバンドルされたテスティングフレームワークの歴史といくつかの主要なRuby用のテスティングフレームワーク（RSpec, minitest, test-unitと関連ソフトウェアをいくつか）の歴史について時系列で紹介します。歴史を知ることで、それぞれのテスティングフレームワークが何を重視しているのか、そして、それが自分が大事にしていることと合致するのかを判断する材料になるはずです。これにより、参加者が「自分にとって適切なテスティングフレームワークを選ぶための知識」を持ち帰ることの実現を目指します。
>
> Rubyにバンドルされたテスティングフレームワークの歴史については↓にまとめたものをベースとします。
http://www.clear-code.com/blog/2014/11/6.html


Pitch:

> 最新のRubyである2.2でtest-unitがバンドルされた背景(\*)を知る人は少ないでしょう。RubyKaigiでのトークとして、RubyのユーザーがRubyの開発の歴史（の一部）を知る機会があるのは妥当だと考えます。
>
> (\*) Test::Unit互換APIを提供するため。そうしないと既存のテストが動かなくなってRubyのバージョンアップの障害になる人がでてバージョンアップの障害になるかもしれない。Python 3のように新しいバージョンがでても古いバージョンを使い続けるユーザーがたくさんいる状況は開発チームとしてはうれしくないので避けたい。
>
> 私はRubyのテスティングフレームワークの歴史に最初から関わっているわけではありません。Ruby 1.8と1.9の間くらいからだけです。しかし、その頃からRubyのテスティングフレームワークまわりについては観測してきましたし、このトークで登場するtest-unit gemテスティングフレームワークの現在のメンテナーです。そのため、私はこのトークをする人として適切だと考えます。


### 応募例：RubyKaigi 2016

RubyKaigi 2016での応募例です。この応募は採択されました。

発表内容：[How to create bindings 2016]({% post_url 2016-09-14-index %})

Title:

> How to create bindings 2016


Abstract:

> This talk describes how to create Ruby bindings of a C library. I want to increase Ruby bindings developers. If you're interested in using C libraries from Ruby and/or using Ruby more cases, this talk will help you.
>
> This talk includes the following topics:
>   * List of methods to create Ruby bindings of a C library.
>   * Small example of each method.
>   * Pros and cons of each method.


Details:

> このトークの目標は参加者が「自分のユースケースにあったRubyバインディングの作り方を知ること」です。まったく知らないという参加者でもわかるようにします。「昔は知っていたけど最近のことはわからない」という参加者でも、2016年時点での最新情報を提供することで、得られるものがあるようにします。
>
> 多くのRubyistはRubyバインディングを作る機会はありませんし、作る必要もありません。しかし、作り方を知っていればいざというときにRubyバインディングを作るという選択肢が生まれ、よりRubyを活用できる可能性が高くなります。
>
> Cライブラリーと連携できることはRubyのよいところの1つです。Cライブラリーと連携できると、Rubyスクリプトを高速化したり、既存のCライブラリーの機能を利用したりできます。速さが足りなくてRubyを使えない、機能が足りなくてRubyを使えない、そのようなときの解決策としてRubyバインディングの作り方が役に立ちます。
>
> このトークではRubyバインディングの作り方として次の方法を紹介します。
>
>   * RubyのC APIを使ったRubyバインディングの作り方
>   * SWIGを使ったRubyバインディングの作り方
>   * libffiを使ったRubyバインディングの作り方
>   * GObject Introspectionを使ったRubyバインディングの作り方
>
> それぞれについて具体的なコードとメリット・デメリットを紹介します。この情報をもって「自分のユースケースに適切なCライブラリーのRubyバインディングの作り方を知ること」の実現を目指します。
>
> 参考URL：[スクリプト言語の拡張機能の作り方とGObject Introspectionの紹介]({% post_url 2013-12-09-index %})


Pitch:

> 多くのRubyistはRubyバインディングを作る機会はありませんし、作る必要もありません。しかし、作り方を知っていればいざというときにRubyバインディングを作るという選択肢が生まれ、よりRubyを活用できる可能性が高くなります。
>
> Cライブラリーと連携できることはRubyのよいところの1つです。Cライブラリーと連携できると、Rubyスクリプトを高速化したり、既存のCライブラリーの機能を利用したりできます。速さが足りなくてRubyを使えない、機能が足りなくてRubyを使えない、そのようなときの解決策としてRubyバインディングの作り方が役に立ちます。
>
> 以上より、Rubyを活用する機会を増やすため、RubyKaigiでRubyバインディングを作る方法を知る機会があるのは妥当だと考えます。
>
> 私は10個以上のRubyバインディングを作った経験があり、このトークで紹介するすべての作り方を使ってきました。そのため、私はこのトークをする人として適切だと考えます。


### 応募例：RubyKaigi 2017

RubyKaigi 2017での応募例です。この応募が採択されるかどうかはわかりません。

Title:

> Improve extension API: C++ as better language for extension


Abstract:

> This talk proposes better extension API.
>
> The current extension API is C API. In the past, some languages such as Rust ([RubyKaigi 2015](http://rubykaigi.org/2015/presentations/wycats_chancancode)), Go ([Oedo RubyKaigi 05](https://speakerdeck.com/naruse/writing-extension-libraries-in-go)), rubex ([RubyKaigi 2016](http://rubykaigi.org/2016/presentations/v0dro.html)) were proposed as languages to write extension.
>
> This talks proposes C++ as a better language for writing extension. Reasons:
>   * C++ API can provide simpler API than C API.
>   * C++ API doesn't need C bindings because C++ can use C API including macro natively. Other languages such as Rust and Go need C bindings.
>   * Less API maintenance cost. Other approaches need more works for Ruby evolution such as introduces new syntax and new API.


Details:

> このトークの目標は参加者が「現在のC APIの課題を知ること」と「このトークで提案するC++ APIがよいかどうかを判断する材料を十分に得ること」です。前者に関しては拡張ライブラリーを書いたことがない人でもわかるようにします。後者に関しては拡張ライブラリーを書いたことががない人には判断できないでしょう。よって、このトークのメインターゲットは拡張ライブラリーを書いたことがある人です。この人たちはこのトークでよりよい拡張ライブラリーのAPIについての知見を得るはずです。拡張ライブラリーを書いたことがない人たちは拡張ライブラリーのAPIそのものについての知見が増えているはずです。
> このトークではC APIの課題として次のことを説明します。
>
>   * 関数のシグネチャーを2回書かないといけず、それらがズレていてもコンパイル時に発見できないため、実行時にクラッシュしてしまう可能性が高まる。
>   * 冗長である。たとえば、メソッドを登録するときにCの関数を定義してそれを別途登録するケース、`each`や`begin rescue`を実現するためにCの関数を定義するケース、などである。
>   * CのデータとRubyのデータ間の変換が面倒であり、入力チェックのコードが多くなってコードの見通しが悪くなり、メンテナンス性が下がる。
>   * Rubyは例外が発生するとlongjumpするので、例外発生前に動的に確保したメモリーの開放処理が煩雑になる。
>
> それぞれについて具体的なコードを紹介します。たとえば、関数のシグネチャーを2回書かないといけないとは次のようなコードのことです。
>
> ```c
> static VALUE
> rb_sample_hello(VALUE self)
> {
>   return rb_str_new_static("Hello");
> }
>
> void
> Init_sample(void)
> {
>   VALUE sample = rb_define_class("Sample", rb_cObject);
>   rb_define_method(sample, "hello", rb_sample_hello, 0);
> }
> ```
>
> このコードでは`hello`メソッドの引数の数が0であることを示すために、`rb_sample_hello(VALUE self)`と関数を定義し、それをRubyに伝えるために`rb_define_method(..., 0)`を実行しています。引数の数が増えた時は両方更新する必要があります。片方の更新を忘れるとクラッシュします。
>
> このような情報をもって「現在のC APIの課題を知ること」の実現を目指します。
>
> それぞれの課題について、課題を解決するC++ APIを提案します。APIだけでなくどうして解決できるのか、および、実装も説明します。このAPIとその解説および実装の説明により「このトークで提案するC++ APIがよいかどうかを判断する材料を十分に得ること」の実現を目指します。
>
> たとえば、関数のシグネチャーを2回書かないといけないケースについては、次のようなC++ APIを提案します。
>
> ```c
> void
> Init_sample(void)
> {
>   rb::Class("Sample", rb_cObject).
>     define_method("hello", [](VALUE self) {return rb_str_new_static("Hello");});
> }
> ```
>
> このAPIではC++のラムダ式`[](VALUE self)`から引数が0であることがわかるため、その情報を自動的にRubyにも伝えます。これにより、シグネチャーの不整合によるクラッシュを防ぐことができます。`[](const char *self)`のようにシグネチャーが不正な時はコンパイル時にエラーにすることもできます。
>
> このような情報をもって「このトークで提案するC++ APIがよいかどうかを判断する材料を十分に得ること」の実現を目指します。
>
> また、RustやGoやrubexなど他のアプローチについても紹介し、それらのアプローチの課題も紹介します。
>
> たとえば、RustやGoを使うアプローチではC APIのマクロの扱いに課題があります。これらの言語ではCのマクロを扱えないため、`RSTRING_LEN()`のようなC APIを使うためにはそれぞれの言語で同様の処理を実現する必要があります。rubexのアプローチではrubexという独自の言語の文法を覚える必要があるという学習コスト面と、rubex自体がRuby本体の進化（たとえば文法の追加）に追従しなければいけないというメンテナンス面の課題があります。
>
> このような情報も「このトークで提案するC++ APIがよいかどうかを判断する材料を十分に得ること」の実現で役立つはずです。
>
> もしかしたら、参考情報としてRubyのC++ APIを提供する[Rice](https://github.com/jasonroelofs/rice)も紹介します。RiceはBoost.PythonのようなAPIを提供します。単なる拡張ライブラリーではなくバインディングを書くときにより便利です。「このトークで提案するC++ APIがよいかどうかを判断する材料を十分に得ること」の実現に役立つ情報なはずです。


Pitch:

> Rubyは3.0で高速化を目指しています。Rubyが高速になることで、より速度を期待するユーザーが増えると予想します。Rubyレベルの高速化で満足できるユーザーも多いでしょうが、さらなる高速化を期待するユーザーも増えるはずです。
>
> そうなったとき、より簡単に拡張ライブラリーを書けるようにすることでより簡単にRubyスクリプトを高速に実行できるようになります。そうなることで、そのようなユーザーも楽しくRubyを使えます。楽しくプログラムを書ける人が増えることはRubyのポリシーとあっています。
>
> このトークで紹介する既存のアプローチは私のアイディアではありませんが、提案するC++ APIの部分は私のアイディアです。
>
> 私は10個以上の拡張ライブラリーを作った経験があり、C APIのよいところも課題も知っています。そのため、私はこのトークをする人として適切だと考えます。

### まとめ

RubyKaigiの発表の応募例として私のここ3年の応募内容を紹介しました。RubyKaigi 2017の[発表の応募](https://cfp.rubykaigi.org/events/2017)は6月17日まで受け付けています。ぜひ応募してみてください。
