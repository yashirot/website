---
tags: []
title: AuFSの代替としてのOverlayFS,unionfs-fuseを使うには
---
### はじめに

Unionファイルシステムと呼ばれる、ファイルシステムを重ねあわせて透過的に扱うための仕組みがあります。この仕組みを実現する実装は複数あり、その一つがAuFS [^0] です。
<!--more-->


AuFSはDebian標準のカーネルではデフォルトで有効になっていたのですが、最近のカーネルでは削除され [^1] ました。

この記事を書いている時点では、以下の2つのlinux-imageが64bit向けに提供されており、AuFSが使えるのは、3.16.0-4のほうのみ [^2] です。

  * linux-image 4.0.0-1

  * linux-image 3.16.0-4

### AuFSの代替としてのOverlayFS,unionfs-fuse

AuFSがDebianの提供している最新のカーネルから削除されたとはいえ、まったく使えないわけではありません。依然としてAuFS自体はカーネル 4.x向けにメンテナンスされています。しかしパッチをあてて自分でビルドするのはしんどいということもあるでしょう。

AuFSの代替案 [^3] はいくつかあります。

  * OverlayFS

  * unionfs-fuse

前者のOverlayFSはカーネル 3.18以降でなら標準で使えます。ただし、モジュールを追加でロードする必要があります。

```sh
sudo modprobe overlay
```


正常にロードできていれば、`/proc/filesystems` にoverlayがいるはずです。

ここで、何故overlayfsじゃないのかと不思議に思った人は[LXCで学ぶコンテナ入門 －軽量仮想化環境を実現する技術 第18回　Linuxカーネルのコンテナ機能 [7] ─ overlayfs](http://gihyo.jp/admin/serial/01/linux_containers/0018)を参照するとよいでしょう。そのあたりの経緯も詳しい説明があります。

後者の `unionfs-fuse` はパッケージを追加でインストールする必要があります。Debianではパッケージも提供されているので、導入も容易です。

```sh
sudo apt-get install unionfs-fuse
```


### マウント方法の違い

準備ができたところで、ディレクトリAとディレクトリBを重ねあわせて使う例を示します。

ディレクトリを重ねあわせる条件は次の通りとします。

  * ディレクトリAは書き込み可能

  * ディレクトリBは読み込み専用

  * 重ねあわせたマウント先はディレクトリB

ディレクトリBに書きこんだら、その内容はディレクトリAにすべて反映される構成です [^4]。

  * AuFSの場合

```sh
sudo mount -t aufs -o br:(ディレクトリA):(ディレクトリB)=ro none (ディレクトリB)
```


  * OverlayFSの場合

```sh
sudo mount -t overlay -o lowerdir=(ディレクトリB),upperdir=(ディレクトリA),workdir=(作業用のディレクトリ) overlay (ディレクトリB)
```


workdirは空のディレクトリである必要があります。

  * unionfs-fuseの場合

```sh
unionfs-fuse (ディレクトリA)=rw (ディレクトリB)
```


AuFSやOverlayFSの場合とくらべると、オプション指定も少なくすっきりしています。
ただし、ユーザーランドでの実装のため、AuFSやOverlayFSに比べると速度の面で劣ります[^5] 。

### まとめ

AuFSの代替として、OverlayFSや `unionfs-fuse` コマンドでディレクトリを重ねあわせて使う方法を紹介しました。
そうそう頻繁に使う機会はないかも知れませんが、知っていると便利なコマンドです。
機会があれば、活用してみてください。

[^0]: http://aufs.sourceforge.net/

[^1]: 3.18-1~exp1以降が該当。

[^2]: Debian Unstableの場合。

[^3]: https://en.wikipedia.org/wiki/UnionFS

[^4]: マスターとなるディレクトリと作業しているディレクトリがわかれていて、作業ディレクトリではマスターとなるディレクトリAのファイルを参照する必要があるという想定です。

[^5]: https://github.com/rpodgorny/unionfs-fuse#why-not-choose-it
