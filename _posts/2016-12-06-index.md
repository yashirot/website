---
tags:
- groonga
- presentation
title: 'PGConf.ASIA 2016：PGroonga – PostgreSQLを全言語対応高速全文検索プラットフォームに！ #pgconfasia'
---
2016年12月1日から12月3日にかけて開催されたPostgreSQLの国際カンファレンス[PGConf.ASIA 2016](http://www.pgconf.asia/)で[PGroonga](https://pgroonga.github.io/ja/)というPostgreSQLの全文検索モジュールの話をしました。
<!--more-->


<div class="rabbit-slide">
  <iframe src="https://slide.rabbit-shocker.org/authors/kou/pgconf-asia-2016/viewer.html"
          width="640" height="524"
          frameborder="0"
          marginwidth="0"
          marginheight="0"
          scrolling="no"
          style="border: 1px solid #ccc; border-width: 1px 1px 0; margin-bottom: 5px"
          allowfullscreen> </iframe>
  <div style="margin-bottom: 5px">
    <a href="https://slide.rabbit-shocker.org/authors/kou/pgconf-asia-2016/" title="PGroonga – Make PostgreSQL fast full text search platform for all languages!">PGroonga – Make PostgreSQL fast full text search platform for all languages!</a>
  </div>
</div>


関連リンク：

  * [スライド（Rabbit Slide Show）](https://slide.rabbit-shocker.org/authors/kou/pgconf-asia-2016/)

  * [スライド（SlideShare）](http://www.slideshare.net/kou/pgconf-asia-2016)

  * [リポジトリー](https://github.com/kou/rabbit-slide-kou-pgconf-asia-2016)

### 内容

前半はPostgreSQLの全文検索まわりの課題の説明とPGroongaがどうやってそれらの課題を解決するかという内容になっています。

後半はPGroongaの説明になっています。

PostgreSQLの全文検索まわりの課題は次の通りです。

  * PostgreSQLが標準で提供する全文検索機能（[textesarch](https://www.postgresql.jp/document/current/html/textsearch.html)と[contrib/pg_trgm](https://www.postgresql.jp/document/current/html/pgtrgm.html)）は日本語や中国語などアジア圏の言語をサポートしていない。（アジア圏の言語以外にもサポートしていない言語はある。）

  * [pg_bigm](http://pgbigm.osdn.jp/)というモジュールを導入することで全言語をサポートできるがヒット数が増えると遅くなりがち。

PGroongaはこれらの課題を解決します。PGroongaは全言語対応でヒット数が増えても高速な全文検索機能を提供します。

以下はpg_bigmとの検索時間の比較です。棒が短いほど速いです。pg_bigm（青い棒）は極端に遅くなるケースがありますが、PGroonga（紫の棒）は安定して高速です。

![PGroongaとpg_bigmの検索時間]({{ "/images/blog/20161206_0.png" | relative_url }} "PGroongaとpg_bigmの検索時間")

この検索時間の比較はデータとして日本語版Wikipediaを用いています。他にもデータとして英語版Wikipediaを用いて、PGroongaとPostgreSQLが標準で提供する全文検索機能を比較したデータもあります。詳細はスライドの内容あるいは以下のドキュメントを参照してください。

  * [PGroonga対textsearch対pg_trgm](https://pgroonga.github.io/ja/reference/pgroonga-versus-textsearch-and-pg-trgm.html)

  * [PGroonga対pg_bigm](https://pgroonga.github.io/ja/reference/pgroonga-versus-pg-bigm.html)

後半のPGroongaの説明では以下のことに触れました。詳細はスライドと以下のリストに含まれているリンクを参考にしてください。

  * PGroongaがなぜ高速か

  * PGroongaで[オートコンプリートを実現](https://pgroonga.github.io/ja/reference/operators/prefix-rk-search-contain-v2.html)する方法（Googleの検索ボックスに「tou」と入れたら「東京」が補完されるような機能の実現方法）

  * [JSON内のすべてのテキストを全文検索](https://pgroonga.github.io/ja/reference/operators/jsonb-query.html)する方法

  * [レプリケーション](https://pgroonga.github.io/ja/reference/replication.html)の実現方法

### まとめ

PGConf.ASIA 2016でPGroongaについて話しました。現在のPostgreSQLの全文検索機能には課題がありますが、PGroongaを組み込むことで、PostgreSQLでアジア圏の言語（もちろん日本語も含む！）でも実用的な全文検索機能を実現することができます。PostgreSQLでの全文検索機能でこまったらPGroongaを検討してみてください。
