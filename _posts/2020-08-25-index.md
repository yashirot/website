---
tags:
- fluentd
title: Windowsの新しいパッケージ管理システムのリポジトリにパッケージを登録するには
---
### はじめに

クリアコードは[Fluentd](http://www.fluentd.org)の開発に参加しています。
<!--more-->


Fluentdにはtd-agentというディストリビューションがあり、Treasure Dataというサービスにすぐに接続しに行けるプラグインをまとめてくれているパッケージやインストーラーが提供されています。
td-agentでは、td-agent 3からWindows向けのインストーラーを提供しており、Windowsへもインストールできます。

### wingetとは

[winget](https://github.com/microsoft/winget-cli)とはMicrosoftが新しく開発したWindows向けのパッケージ管理システムです。

このwingetはフリーソフトウェアとして公開されており、また、パッケージを登録するリポジトリやプロセスも公開されています。
winget本体は[microsoft/winget-cli](https://github.com/microsoft/winget-cli)リポジトリとして管理され、wingetのパッケージは[microsoft/winget-pkgs](https://github.com/microsoft/winget-pkgs)として公開されています。

### wingetに登録するパッケージマニフェストを作成する

winget-pkgsのリポジトリをcloneしてきます。

```console?lang=powershell
PS> git clone git@github.com:microsoft/winget-pkgs.git
```


cloneしたwinget-pkgsにはTools以下のフォルダに[YamlCreate.ps1](https://github.com/microsoft/winget-pkgs/blob/master/Tools/YamlCreate.ps1)という対話的にYAMLのマニフェストを作成するPowerShellスクリプトが用意されています。
また、winget-pkgsをforkしてoriginとしてリモートリポジトリを登録しておきます。

### 実際にマニフェストを作成してみる

実際にtd-agent 4.0.1のマニフェストを作成したログは以下の通りです。

```console?lang=powershell
PS> .\Tools\YamlCreate.ps1
Enter the URL to the installer: http://packages.treasuredata.com/4/windows/td-agent-4.0.1-x64.msi


Downloading URL.  This will take awhile...
Url: http://packages.treasuredata.com/4/windows/td-agent-4.0.1-x64.msi
Sha256: E9BEBBDB2FF488583DD779505A714C44560CA16499EFDCC571E1A14E18BD383C


File downloaded. Please Fill out required fields.
Enter the package Id, in the following format <Publisher.Appname>
For example: Microsoft.Excel: TreasureData.TDAgent
Enter the publisher: Treasure Data Inc.
Enter the application name: Treasure Agent
Enter the version. For example: 1.0, 1.0.0.0: 4.0.1
Enter the License, For example: MIT, or Copyright (c) Microsoft Corporation: Apache Lincense 2.0
Enter the InstallerType. For example: exe, msi, msix, inno, nullsoft: msi
Enter the architecture (x86, x64, arm, arm64, Neutral): x64
[OPTIONAL] Enter the license URL: https://www.apache.org/licenses/LICENSE-2.0
[OPTIONAL] Enter the AppMoniker (friendly name). For example: vscode: td-agent
[OPTIONAL] Enter any tags that would be useful to discover this tool. For example: zip, c++: fluentd, logging
[OPTIONAL] Enter the Url to the homepage of the application: https://www.fluentd.org
[OPTIONAL] Enter a description of the application: A data collector for Treasure Data.


Id: TreasureData.TDAgent
Version: 4.0.1
Name: Treasure Agent
Publisher: Treasure Data Inc.
License: Apache Lincense 2.0
LicenseUrl: https://www.apache.org/licenses/LICENSE-2.0
AppMoniker: td-agent
Tags: fluentd, logging
Description: A data collector for Treasure Data.
Homepage: https://www.fluentd.org
Arch: x64
Url: http://packages.treasuredata.com/4/windows/td-agent-4.0.1-x64.msi
Sha256: E9BEBBDB2FF488583DD779505A714C44560CA16499EFDCC571E1A14E18BD383C
InstallerType: msi
Yaml file created:  C:\Users\cosmo\Documents\GitHub\winget-pkgs\manifests\TreasureData\TDAgent\4.0.1.yaml
Now place this file in the following location: \manifests\TreasureData\TDAgent
```


この一連の対話的な操作により、td-agent 4.0.1のマニフェストが`~\GitHub\winget-pkgs\manifests\TreasureData\TDAgent\4.0.1.yaml`に作成されました。

動作確認をしてみます。

```console?lang=powershell
PS> winget validate .\manifests\TreasureData\TDAgent\4.0.1.yaml
マニフェストの検証は成功しました。
PS> winget install -m .\manifests\TreasureData\TDAgent\4.0.1.yaml
Found Treasure Agent [TreasureData.TDAgent]
このアプリケーションは所有者からライセンス供与されます。
Microsoft はサードパーティのパッケージに対して責任を負わず、ライセンスも付与しません。
Downloading http://packages.treasuredata.com/4/windows/td-agent-4.0.1-x64.msi
  ██████████████████████████████  24.0 MB / 24.0 MB
インストーラーハッシュが正常に検証されました
パッケージのインストールを開始しています...
```


`winget validate /path/to/manifest.yml`も`winget install -m /path/to/manifest.yml`も検証できたようです。

変更をコミットします。

```console?lang=powershell
PS> git checkout -b td-agent-4.0.1
PS> git add manifests/TreasureData/TDAgent/4.0.1.yaml
PS> git commit -s
[td-agent-4.0.1 545f751] Add Treasure Agent 4.0.1
 1 file changed, 18 insertions(+)
 create mode 100644 manifests/TreasureData/TDAgent/4.0.1.yaml
```


forkしたリモートリポジトリにpushします。

```console?lang=powershell
PS> git push origin td-agent-4.0.1
Counting objects: 6, done.
Delta compression using up to 8 threads.
Compressing objects: 100% (5/5), done.
Writing objects: 100% (6/6), 837 bytes | 418.00 KiB/s, done.
Total 6 (delta 2), reused 0 (delta 0)
#...
```


### winget-pkgsのリポジトリにマニフェストを登録する

forkしたリモートリポジトリに作成したマニフェストをpushしたらfork元の[microsoft/winget-pkgs](https://github.com/microsoft/winget-pkgs)に対してPull Requestを作成します。

今回作成したマニフェストは [microsoft/winget-pkgs#3153](https://github.com/microsoft/winget-pkgs/pull/3153) として登録しました。

td-agentは現在署名をしていないパッケージなので、[Microsoft Defender SmartScreenの警告](https://docs.microsoft.com/ja-jp/windows/security/threat-protection/microsoft-defender-smartscreen/microsoft-defender-smartscreen-overview)に引っかかってしまうため少しマージまでに時間がかかりましたが、無事に取り込まれました。

[microsoft/winget-pkgs](https://github.com/microsoft/winget-pkgs)にマニフェストが取り込まれてしばらくすると、マニフェスト情報が更新され、td-agent 4.0.1が`winget search`で引っかかるようになりました。

```console?lang=powershell
PS> winget search td-agent
名前             ID                   バージョン 一致
-----------------------------------------------------------
Treasure Agent TreasureData.TDAgent 4.0.1 Moniker: td-agent
```


インストールすることもできます。

```console?lang=powershell
PS> winget install td-agent
Found Treasure Agent [TreasureData.TDAgent]
このアプリケーションは所有者からライセンス供与されます。
Microsoft はサードパーティのパッケージに対して責任を負わず、ライセンスも付与しません。
Downloading http://packages.treasuredata.com/4/windows/td-agent-4.0.1-x64.msi
  ██████████████████████████████  24.0 MB / 24.0 MB
インストーラーハッシュが正常に検証されました
パッケージのインストールを開始しています...
インストールが完了しました 
```


### まとめ

FluentdのWindows周りの作業はクリアコードが得意としている領域です。td-agentのインストールがより手軽にできるようになれば、結果的にWindowsでのFluentdやtd-agentのユーザーを増やす事ができます。
この取り組みはWindowsにおいてtd-agentをより簡易に導入できるようにする施策の一環として実施されました。

当社では、お客さまからの技術的なご質問・ご依頼に有償にて対応する[Fluentdサポートサービス](/services/fluentd.html)を提供しています。Fluentd/Fluent Bitをエンタープライズ環境において導入/運用されるSIer様、サービス提供事業者様は、[お問い合わせフォーム](/contact/)よりお問い合わせください。
