---
tags:
- fluentd
title: Fluentd v0.14で導入されたstorageプラグインとは
---
### はじめに

Fluentd v0.14では新たにstorageプラグインという新しいタイプのプラグインが導入されました。
FluentdをインストールしただけではJSON形式により保存される `storage_local` プラグインしかありませんが、このstorageプラグインはFluentdのプラグインのインスタンスが保持する値をKVSに集約することにも使用することができます。
<!--more-->


### storageプラグインとstorageプラグインヘルパー

Fluentd v0.14ではさらにプラグインヘルパーという概念も追加されました。storageプラグインにおいてもstorageプラグインを直接使うのではなく、storageプラグインヘルパーを通じて使うことが推奨されます。

### storageプラグインのAPI

storageプラグインは以下のAPIを持ちます。これはKVSから値を取り出したり、保存したり、また取り出した値をキャッシュしておくのに合うAPIとなっています。

```ruby
# basically, interact with KVS
def load
end

# basically, interact with KVS
def save
end

# Normally, the following methods work as `cache`.
def get(key)
end

def fetch(key, defval)
end

def put(key, value)
end

def delete(key)
end

def update(key, &block) # transactional get-and-update
end
```


このうち、 `#load` と `#save` については実際のKVSに対して値を読み込んできたり、保存したりする役割を担います。
一方、 `#get`、`#fetch`、`#put`、`#delete`、`#update`についてはstorageプラグインだけではキャッシュとして振る舞うことが求められます。

### storageプラグインヘルパー

storageプラグインヘルパーはstorageプラグインを直接使わずにstorageプラグインの性質を変化させるように作成されています。
storageプラグインヘルパーの `#wrap_instance` メソッドにより、storageプラグインのインスタンスをそのまま使用するか、storageプラグインの値を永続化して同期を取るか、単に同期を取るかが決定されます。

Fluentdの実際のコードでは以下のようになっています。

```ruby
def wrap_instance(storage)
  if storage.persistent && storage.persistent_always?
    storage
  elsif storage.persistent
    PersistentWrapper.new(storage)
  elsif !storage.synchronized?
    SynchronizeWrapper.new(storage)
  else
    storage
  end
end
```


`<storage>` セクションに `persistent true` が設定されていることや、 `#persistent_always?` の返す値、`#synchronized?` が返す値が振る舞いを変えることがわかります。

### 実際のstorageプラグインの例

これらを踏まえて、筆者はMongo、Redis、Memchachedについてのstorageプラグインをそれぞれ作成しました。

  * [fluent-plugin-storage-mongo](https://github.com/cosmo0920/fluent-plugin-storage-mongo)

  * [fluent-plugin-storage-redis](https://github.com/cosmo0920/fluent-plugin-storage-redis)

  * [fluent-plugin-storage-memcached](https://github.com/cosmo0920/fluent-plugin-storage-memcached)

これらを用いると上記3つのKVSに対してstorageプラグインによりownerプラグインであるinput, output, filterプラグインの情報をKVSへ集約することができます。

### まとめ

Fluentd v0.14で導入されたstorageプラグインの概要とstorageプラグインヘルパーを通した場合の振る舞いの変化について解説しました。
storageプラグインをうまく活用するとstorageプラグイン対応が入っている[fluent-plugin-systemd](https://github.com/reevoo/fluent-plugin-systemd)や[fluent-plugin-windows-eventlog](https://github.com/fluent/fluent-plugin-windows-eventlog)のようにどこまで読んだかの位置の記録をKVSに集約することができるようになります。
