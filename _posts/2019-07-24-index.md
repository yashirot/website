---
tags:
- fluentd
title: Fluent-bit-go-s3とFluent BitのGo Pluginプロキシの話
---
### はじめに

[Fluent Bit](https://fluentbit.io/)はFluentdファミリーを構成するソフトウェアの一つです。
Fleunt BitのWindows対応はプラグインの対応だけではなく、Go Pluginプロキシについても対応を行っています。
<!--more-->


Fluent BitのGo Pluginプロキシは[v1.1.0からWindowsでも利用可能](https://github.com/fluent/fluent-bit/commit/8083d5192c6175406a635032ca0d8ee73df755de)です。

### Fluent BitのGo Pluginプロキシとは

Fluent Bitは共有オブジェクトファイルを読み込んでOutputプラグインとして振る舞わせることができます。

例えば、Golangを使って共有オブジェクトファイルを作成する場合のコードは以下のようになります。

```go
package main

import "github.com/fluent/fluent-bit-go/output"

//export FLBPluginRegister
func FLBPluginRegister(def unsafe.Pointer) int {
    // Gets called only once when the plugin.so is loaded
	return output.FLBPluginRegister(ctx, "gskeleton", "Hey GO!")
}

//export FLBPluginInit
func FLBPluginInit(plugin unsafe.Pointer) int {
    // Gets called only once for each instance you have configured.
    return output.FLB_OK
}

//export FLBPluginFlushCtx
func FLBPluginFlushCtx(ctx, data unsafe.Pointer, length C.int, tag *C.char) int {
    // Gets called with a batch of records to be written to an instance.
    return output.FLB_OK
}

//export FLBPluginExit
func FLBPluginExit() int {
	return output.FLB_OK
}

func main() {
}
```


このコードを用いて以下のようにするとGolangで共有オブジェクトを作成することができます。

```bash
$ go build -buildmode=c-shared -o out_skeleton.so .
```


### GolangをOutputプラグインに使う利点

GolangでFluent BitのOutputプラグインに使う利点はC言語向けには提供されていないライブラリであり、Golang向けには提供されているライブラリが使用できることです。
例えば、[Golang向けにはAWS SDKが提供](https://github.com/aws/aws-sdk-go)されています。

このSDKを用いてFluent BitからAWSのサービスへ直接接続することができます。

### Golang製Fluent Bitプラグインの例

Golang製のFluent Bitプラグインの例として、[AWS S3へFluent Bitから直接接続できるようにするプラグイン](https://github.com/cosmo0920/fluent-bit-go-s3)を実際に作成しました。
GolangはWindowsではdllを作成することが可能です。Windows向けのDLLに関しては[リリースページに置いて](https://github.com/cosmo0920/fluent-bit-go-s3/releases)あります。

### まとめ

Fluent BitのGo Pluginプロキシとその実例を紹介しました。Golang製であれば共有オブジェクトとしてもさほど依存が含まれません。
例として、Debian 10上でGo 1.12.7でビルドした場合には以下の参照が共有オブジェクトへ含まれます。

```bash
% ldd out_s3.so
	linux-vdso.so.1 (0x00007ffd615ca000)
	libpthread.so.0 => /lib/x86_64-linux-gnu/libpthread.so.0 (0x00007f6e182a3000)
	libc.so.6 => /lib/x86_64-linux-gnu/libc.so.6 (0x00007f6e180e2000)
	/lib64/ld-linux-x86-64.so.2 (0x00007f6e19933000)

```


Fluent BitのGolang製のプラグインという選択肢が増えたことにより、AWS SDKを使ってAWSのサービスへ直接接続するプラグインを開発することが出来るようになりました。
依存ライブラリもGolangのみであれば一つのソースでWindowsで動くDLLの作成やLinuxで動作する共有オブジェクトの作成や、macOSでの動的ロードライブラリの作成ができます。そしてそれらをFluent Bit読み込ませて動かすことができます。ぜひ試してみてください。
