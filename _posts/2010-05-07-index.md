---
tags: []
title: groonga用Muninプラグイン
---
まだリリース版に入っていませんが、[groongaのリポジトリにはMuninプラグイン](http://github.com/groonga/groonga/tree/master/data/munin/)が入っています。
<!--more-->


プラグインは5種類あり、それぞれ以下の値をグラフにします。

  * groonga_cpu_load: groongaサーバのCPU使用率をグラフにします。
  * groonga_cpu_time: groongaサーバがCPUを使った時間をグラフにします。
  * groonga_memory_usage: groongaサーバが使用しているメモリをグラフにします。
  * groonga_n_records: 各テーブルのレコード数をグラフにします。
  * groonga_status: groonga内部でのメモリ割り当て数をグラフ
    にします。（デバッグ用。groonga内部でメモリリークしていないかを
    確認するときなどに使う。）

グラフの実例: [rurema.clear-code.comのもの](http://rurema-munin.clear-code.com/rurema.clear-code.com.html#Groonga)

必ずしもすべてのプラグインを使った方がよいというわけではありません。ユースケース毎に利用した方がよいプラグインは以下の通りです。

<dl>






<dt>






groongaサーバを利用している場合






</dt>






<dd>


  * groonga_cpu_load
  * groonga_cpu_time
  * groonga_memory_usage
  * groonga_n_records


</dd>








<dt>






rroongaで直接データベースを操作するなどgroongaサーバを利用していない場合






</dt>






<dd>


[るりまサーチ](http://rurema.clear-code.com/)はこのケースです。


  * groonga_n_records


</dd>








<dt>






groongaサーバを利用していて、groonga内部にも手を入れている場合






</dt>






<dd>


  * groonga_cpu_load
  * groonga_cpu_time
  * groonga_memory_usage
  * groonga_n_records
  * groonga_status


</dd>


</dl>

### インストール方法

これらのプラグインは、groonga（のHEAD）をインストールすると$PREFIX/share/groonga/munin/plugins/以下にインストールされます。例えば、以下のようにインストールした場合は$HOME/local/share/groonga/munin/plugins/以下にインストールされます。

{% raw %}
```
% git clone git://github.com/groonga/groonga.git
% cd groonga
% ./autogen.sh
% ./configure --prefix=$HOME/local
% make
% make install
```
{% endraw %}

まず、インストールされたプラグインのシンボリックリンクをMuninのプラグイン置き場へ置きます。Debian GNU/Linuxでは/usr/share/munin/plugins/がMuninのプラグイン置き場なので以下のようになります。

{% raw %}
```
% cd /usr/share/munin/plugins
% sudo ln -fs ~/local/share/groonga/munin/plugins/* ./
```
{% endraw %}

groonga_n_recordsとgroonga_statusはRubyで書かれていて、JSONライブラリを必要とします。これらのプラグインを使う場合はRubyとRubyのJSONライブラリをインストールしておく必要があります。Debian GNU/Linuxの場合は以下のようにインストールできます。

{% raw %}
```
% sudo aptitude -V -D -y install libjson-ruby
```
{% endraw %}

### 設定方法

次に、プラグインの設定をします。Debian GNU/Linuxであれば、以下のような内容の/etc/munin/plugin-conf.d/groonga.confを作成します。（groongaサーバを起動していて、起動中のgroongaサーバのプロセスIDが/var/run/groonga/groonga.pidに書いてある場合。）

{% raw %}
```
[groonga_*]
  env.pid_file /var/run/groonga/groonga.pid
```
{% endraw %}

例えば、以下のように起動すると、/var/run/groonga/groonga.pidにプロセスIDが記録されます。

{% raw %}
```
% sudo mkdir -p /var/run/groonga
% sudo chown groonga:groonga /var/run/groonga
% groonga -d /var/lib/groonga/db 2>/var/run/groonga/groonga.pid
```
{% endraw %}

この設定をすることで、以下の3つのプラグインを利用できます。

  * groonga_cpu_load
  * groonga_cpu_time
  * groonga_memory_usage

また、gqtp（groonga独自プロトコル）でgroongaサーバを起動していれば以下のプラグインも利用できます。

  * groonga_n_records
  * groonga_status

もし、標準以外のポートでgroongaサーバを起動しているときはenv.portも設定する必要があります。以下はgroongaサーバを20041番ポートで起動している場合の例です。

{% raw %}
```
[groonga_*]
  env.port 20041
```
{% endraw %}

rroongaなど直接データベースを操作している場合は、同じデータベースを指定してgroongaサーバを起動してください。groongaは複数のプロセスで同じデータベースを操作することができるので、rroongaで操作しているデータベースの内容をgroongaサーバから参照することも問題なくできます。

例えば、~/local/var/lib/groonga/dbにデータベースを置いている場合は以下のようにして、Munin用にgroongaサーバを起動できます。

{% raw %}
```
% groonga -i 127.0.0.1 -d ~/local/var/lib/groonga/db
```
{% endraw %}

上記のように/usr/bin以外の場所にgroongaをインストールした場合はenv.groongaでgroongaコマンドのパスを指定します。

{% raw %}
```
[groonga_*]
  env.groonga /home/groonga/local/bin/groonga
```
{% endraw %}

### プラグインの有効化

設定ファイルを作成したら必要なものだけ有効にします。

これらのプラグインはautoconfに対応しているので（参考: [Muninプラグインの作り方]({% post_url 2010-04-08-index %})）、以下のコマンド（Debian GNU/Linuxの場合）で必要なものだけインストールできます。

{% raw %}
```
% sudo -H /usr/sbin/munin-node-configure --shell --remove-also | sudo -H sh
```
{% endraw %}

munin-nodeの再起動を忘れないようにしましょう。

{% raw %}
```
% sudo /etc/init.d/munin-node restart
```
{% endraw %}

### まとめ

最近追加されたgroonga用のMuninプラグインのインストール方法を紹介しました。

groonga_memoryなどはgroonga専用ではなく、もっと汎用的に利用できます。groongaのMuninプラグインを参考にするなど、自分用のMuninプラグインを作ってみてはいかがでしょうか。

あわせて読みたい: [Muninプラグインの作り方]({% post_url 2010-04-08-index %})
