---
layout: default
main_title: OSCオンラインSpringにて、Fluentdで取り組んでいる継続的にOSS開発できるようにする仕組みづくりと最新開発情報を発表します。
sub_title:
type: topic
---

2022年3月11日（金）～12日（土）に開催される、Open Source Conference 2022 Online Springにて、『OSSを継続的にメンテナンスしていく仕組みづくり – Fluentdの事例とその最新情報』と題して発表します。

クリアコードでは2021年から、世界中で利用されているログ収集OSS"Fluentd"をそれまでの属人的な開発体制から、長期的に開発・維持していけるコミュニティでの開発体制を構築していくためにツールの整備などを行いつつ、開発をリードしてきました。発表ではFluentdコアメンテナを務める林健太郎・足永拓郎の二人が、コミュニティ運営をしていくための具体的な課題抽出・改善のための取り組みや、最新のFluentd開発情報などを紹介します。


## 発表概要
* タイトル:『OSSを継続的にメンテナンスしていく仕組みづくり – Fluentdの事例とその最新情報』
* 日時: 2022年3月12日(土) 14:00 〜 14:45
* 場所： D会場　（詳しくはイベントページをご確認ください）
* 発表資料:
  * https://gitlab.com/ashie/rabbit-slide-ashie-osc2022-online-spring-fluentd
  * https://github.com/kenhys/rabbit-slide-kenhys-osc2022-online-spring-fluentd
* 対象者:
  * OSSを継続的に開発していくことに興味がある人 
  * Fluentdを利用している・したことのある人 
  * Fluentdの最新情報を知りたい人
* 前提知識:
  * OSSに何らかの形で関わったことがある方(例: GitHubのIssueを参照したことがある・報告したことがある・PRを送ったことがある) 
  * サーバーの運用等でログを取り扱ったことがある方



## Open Source Conference 2022 Online Springイベント概要
* 日時: 2022年3月11日（金）～12日（土）
* 会場: オンライン会場（Zoom＆YouTube Live）
* 参加費: 無料
* web: https://event.ospn.jp/osc2022-online-spring/
* 参加申し込み（conpass）: https://ospn.connpass.com/event/231342/
